import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  // You can add global styles to this file, and also import other style files
  'body': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'maxWidth': [{ 'unit': 'px', 'value': 1200 }],
    'margin': [{ 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'string', 'value': 'auto' }]
  },
  'status': {
    'width': [{ 'unit': 'px', 'value': 15 }],
    'height': [{ 'unit': 'px', 'value': 15 }],
    'borderRadius': '50%',
    'display': 'block'
  },
  'red': {
    'backgroundColor': 'red'
  },
  'green': {
    'backgroundColor': 'green'
  },
  'yellow': {
    'backgroundColor': '#F9DF37'
  },
  'filter-tab-wrapper': {
    'width': [{ 'unit': 'px', 'value': 316 }],
    'display': 'flex'
  },
  'fiter-tab': {
    'display': 'block',
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#169BD5' }],
    'textAlign': 'center'
  },
  'filter-square-input': {
    'maxWidth': [{ 'unit': 'px', 'value': 60 }]
  }
});
