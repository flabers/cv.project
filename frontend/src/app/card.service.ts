import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

@Injectable()
export class CardService {
  constructor(private http: Http/* , private authService: AuthService */) {

  }

    getRealty(id: number) {
        const body = JSON.stringify({id: id});
        const headers = new Headers({
            'Content-Type': 'application/json',
            //'Cache-Control': 'no-cache'
    });
        return this.http.post('http://realty/api/card' , body, {headers: headers})
            .map(
                (response: Response) => response.json()
            );
    }
    updateData(id: number, newSquare: string, colume: any, name: string, action: any, user_id: number) {
        const body = JSON.stringify({
            value: newSquare,
            id: id,
            colume: colume, //name of colume in DB
            name: name, //table name
            data: action,
            user_id: user_id
        });
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.put('http://realty/api/update-realty', body, {headers: headers})
            .map(
                (response: Response) => response.json().message
            );
    }
    saveComment(user_id, realty_id, comment) {
        const body = JSON.stringify({
            user_id: user_id,
            realty_id: realty_id,
            comment: comment
        });
        console.log(body);
        const headers = new Headers({
            'Content-Type': 'application/json',
            //'Cache-Control': 'no-cache'
        });
        return this.http.post('http://realty/api/comment' , body, {headers: headers})
            .map(
                (response: Response) => response.json()
            );
    }

}
