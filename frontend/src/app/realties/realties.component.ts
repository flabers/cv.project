import { Component, OnInit } from '@angular/core';

import { HeaderComponent } from '../header/header.component';
import { RealtyService } from '../realty.service';
import { Realty } from '../realty.interface';
import { Response } from '@angular/http';

@Component({
  selector: 'app-realties',
  templateUrl: './realties.component.html',
  styleUrls: ['./realties.component.css']
})
export class RealtiesComponent implements OnInit {

  realties: Realty[];
  conclusion: any;

  constructor(private realtyService: RealtyService) { }

  ngOnInit() {
    this.realtyService.getRealties().subscribe(
        (realties) => {
          this.realties = realties.realties;
          this.conclusion = {
              'total': realties.total,
              'not_reliable': realties.not_reliable,
              'rented': realties.rented,
              'not_rented': realties.not_rented,
              'users': realties.users,
              'statuses': realties.statuses,
              'districts': realties.district
          }
        },
        (error: Response) => console.log(error)
    );

  }
}
