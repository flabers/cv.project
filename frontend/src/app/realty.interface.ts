export interface Realty {
  status_id: number;
  address: string;
  district: string;
  user_id: number;
  realty_id: string;
  squere: number;
  type_id: number;
  tenant_name: string;
  created_at: string;
  type_name: string;
  updated_at: string;
}
