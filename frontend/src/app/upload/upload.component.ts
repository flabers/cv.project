import { Component, OnInit, NgModule, Input } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser'
import {UploadService} from "../upload.service";

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  @Input() realty;
  name: string = '';
  fileList: any;
  realty_pictures: any;
  constructor(private uploadService: UploadService) {
    this.fileList = [];

  }
  ngOnInit() {
    console.log(this.realty.realty_id);
  }
  fileUpload(event) {
    this.fileList.push(event.target.files);
  }
  upLoadPictureOnServer() {
    this.uploadService.upload(this.fileList, this.realty.realty_id)
        .subscribe(
        (realty_pictures) => {
          realty_pictures = realty_pictures;
        }
    );{

    }
  }
}
