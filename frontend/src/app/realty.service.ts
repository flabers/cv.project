import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

@Injectable()
export class RealtyService {
  constructor(private http: Http/* , private authService: AuthService */) {

  }

  getRealties(): Observable<any> {
    return this.http.get('http://realty/api/home')
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

}
