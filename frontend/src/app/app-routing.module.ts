import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { RealtiesComponent } from './realties/realties.component';
import {UploadComponent} from "./upload/upload.component";

const routes: Routes = [
  { path: '', component: RealtiesComponent },
  {
    path: 'card/:id',
    component: CardComponent
  },
  {
    path: 'upload',
    component: UploadComponent
  }


];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
