import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import {NgForm} from '@angular/forms';

import { AppComponent } from './app.component';
import { CartComponent } from './cart/cart.component';
import { CardComponent } from './card/card.component';
import { HeaderComponent } from './header/header.component';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { HomestatusComponent } from './homestatus/homestatus.component';
import { AppRoutingModule } from './app-routing.module';
import { FilterComponent } from './filter/filter.component';
import { RealtyComponent } from './realty/realty.component';
import { SortComponent } from './sort/sort.component';
import { RealtyService } from './realty.service';
import { RealtytableComponent } from './realtytable/realtytable.component';
import  {CardService} from './card.service';
import { RealtiesComponent } from './realties/realties.component';
import { ConclusionComponent } from './conclusion/conclusion.component';
import { UserService } from "./user.service";
import { SearchService } from  './search.service';
import { UploadComponent } from './upload/upload.component';
import {UploadService} from "./upload.service";

@NgModule({
  declarations: [
    AppComponent,
    CartComponent,
    CardComponent,
    HeaderComponent,
    SignupComponent,
    SigninComponent,
    HomestatusComponent,
    FilterComponent,
    RealtyComponent,
    SortComponent,
    RealtytableComponent,
    RealtiesComponent,
    ConclusionComponent,
    UploadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    RealtyService,
    CardService,
    UserService,
    SearchService,
    UploadService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
