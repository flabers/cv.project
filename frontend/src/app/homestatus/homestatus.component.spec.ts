import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomestatusComponent } from './homestatus.component';

describe('HomestatusComponent', () => {
  let component: HomestatusComponent;
  let fixture: ComponentFixture<HomestatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomestatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomestatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
