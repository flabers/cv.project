import { Component, OnInit } from '@angular/core';

import { HeaderComponent } from './header/header.component';
import { RealtyService } from './realty.service';
import { Realty } from './realty.interface';
import { Response } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

}
