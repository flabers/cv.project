import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';
@Injectable()
export class SearchService {
    constructor(private http: Http/* , private authService: AuthService */) { }
    getSearch(str: any) {
        const body = JSON.stringify({
            search: str
        });
        const headers = new Headers({
            'Content-Type': 'application/json',
            //'Cache-Control': 'no-cache'
        });
        return this.http.post('http://realty/api/card' , body, {headers: headers})
            .map(
                (response: Response) => response.json()
            );
    }
}
