import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';
@Injectable()
export class UploadService {
    constructor(private http: Http/* , private authService: AuthService */) { }
    upload(fileList: any, realty_id){
        let num:number = 0;

        for(num=0;num<fileList.length; num++){
            let formData:FormData = new FormData();
            let file: File = fileList[num][0];
            formData.append('photo', file, file.name);
            formData.append('realty_id', realty_id);
            let headers = new Headers();
            headers.append('Accept', 'application/json');
            headers.append('Authorization','Bearer ' + localStorage.token );

            let options = new RequestOptions({ headers: headers });
            return this.http.post('http://realty/api/picture', formData, options)
                .map(res => res.json())
                .catch(error => Observable.throw(error))
        }


    }
}
