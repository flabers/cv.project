import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { CardComponent } from './card/card.component';
import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { HomestatusComponent } from './homestatus/homestatus.component';



const APP_ROUTES: Routes = [
  {
    path: '',
    component: AppComponent
  },
  {
    path: 'card/:id',
    component: CardComponent
  },
  {
    path: 'homestatus',
    component: SignupComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'signin',
    component: SigninComponent
  }
];
export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
