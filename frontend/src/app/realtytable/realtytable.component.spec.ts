import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealtytableComponent } from './realtytable.component';

describe('RealtytableComponent', () => {
  let component: RealtytableComponent;
  let fixture: ComponentFixture<RealtytableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealtytableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealtytableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
