import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-conclusion',
  templateUrl: './conclusion.component.html',
  styleUrls: ['./conclusion.component.css']
})
export class ConclusionComponent implements OnInit {
  @Input() conclusion;
  constructor() { }

  ngOnInit() {
  }

}
