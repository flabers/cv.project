import { Component, OnInit } from '@angular/core';
import {UserService} from "../user.service";
import {SearchService} from "../search.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user_name: string = '';
  search_result: any = [];
  constructor(private userService: UserService, private searchService: SearchService) { }


  ngOnInit() {

  }
  values = '';

  onKey(event: any) { // without type info
    if (event.target.value.length > 1) {
      this.values = event.target.value;
        this.searchService.getSearch(this.values)
            .subscribe(
            (result) => {
              this.search_result = result.realties;
            }
        );
    }
    }

  }

