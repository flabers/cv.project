import { Component, OnInit,Output, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CardService} from '../card.service';
import { UploadComponent } '../upload/upload.component';
import { Response } from '@angular/http';
import {NgForm} from '@angular/forms';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit, OnDestroy {
  id: number;
  private sub: any;
  realty_pictures: any;
  show_upolad_form = false;

  square_editing = false;
  editValueSquare = '';
  oldSquareValue = '';

  address_editing = false;
  editValueAddress = '';
  oldValueAddress = '';

  //districts
  onValueDistrict = '';
  district_editing = false;
  oldValueDistrict = '';
  //type
  onValueType = '';
  type_editing = false;
  type_name = '';
  oldValueType = '';
  //status
  status_editing = false;
  //tenant
  editValueTenant: string = '';
  tenant_editing = false;
  oldValueTenant: string = '';

  //comment
  commentValue: string = '';
  comments: any = [];
  constructor(private route: ActivatedRoute, private cardService:  CardService) {
    route.params.subscribe(val => this.initialize());
  }
  realty;
  details: any;

  ngOnInit() {

  }
  initialize(){
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number

      // In a real app: dispatch action to load the details here.
    });
    this.cardService.getRealty(this.id).subscribe(
        (realty) => {
          this.realty = realty.realty;
          this.editValueSquare = this.realty.squere;
          this.details = {
            'logs': realty.logs,
            'comments': realty.comments,
            'days_left': realty.days_left,
            'districts': realty.district,
            'statuses': realty.statuses,
            'types': realty.types,
            'realty_pictures': realty.realty_pictures
          }
        },
        (error: Response) => console.log(error)
    );
  }
  showUploadForm() {
      if (this.show_upolad_form) {
          this.show_upolad_form = false;
      } else {
          this.show_upolad_form = true;
      }
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  //Square edit
  onEditSquare() {
    this.square_editing = true;
    this.oldSquareValue = this.realty.squere;
    this.editValueSquare = this.realty.squere;

  }
  onSaveSquare() {
    const action = {
      'name': 'Смена площади',
      'old': this.oldSquareValue,
      'new': this.editValueSquare
    };
    this.cardService.updateData(this.realty.id, this.editValueSquare, 'squere', 'realties', action, this.realty.user_id)
        .subscribe(
            (square) => {
              alert(square);
            }
        );
    this.square_editing = false;
  }
  onCancelSquare() {
    this.square_editing = false;
  }
  //Address edit

  onEditAddress() {
    this.address_editing = true;
    this.editValueAddress = this.realty.address;
    this.oldValueAddress = this.realty.address;
  }
  onSaveAddress() {
    const action = {
      'name': 'Смена адреса',
      'old': this.oldValueAddress,
      'new': this.editValueAddress
    };
    this.cardService.updateData(this.realty.id, this.editValueAddress, 'address', 'realties', action, this.realty.user_id)
        .subscribe(
            (address) => {
              alert(address);
            }
        );
    this.address_editing = false;
  }
  onCancelAddress() {
    this.address_editing = false;
    this.realty.address = this.editValueAddress;
  }

  //district edit

  onOpenEditDistrict() {
    this.district_editing = true;
  }
  onSaveDistrict() {
    const action = {
      'name': 'Смена адреса',
      'old': this.oldValueDistrict,
      'new': this.onValueDistrict
    };
    this.cardService.updateData(this.realty.id, this.onValueDistrict, 'district', 'realties', action, this.realty.user_id)
        .subscribe(
            (district) => {
              this.realty.district = district;
              alert(district);
            }
        );
    this.district_editing = false;
  }
  onCancelDistrict() {
    this.district_editing = false;
  }
  onChangeDistrict(event) {
    this.onValueDistrict = event;
    this.oldValueDistrict = event;
  }

  //type

  onOpenEditType() {
    this.type_editing = true;
  }
  onSaveType() {
    const action = {
      'name': 'Смена типа',
      'old': this.oldValueType,
      'new': this.type_name
    };
    this.cardService.updateData(this.realty.id, this.onValueType, 'type_id', 'realties', action, this.realty.user_id)
        .subscribe(
            (type) => {
              this.realty.type_name = this.type_name;
              alert(type);
            }
        );
    this.type_editing = false;
  }
  onCancelType() {
    this.type_editing = false;
  }
  onChangeType(event) {
    this.onValueType = event.id;
    this.type_name = event.type_name;
    this.oldValueType = event.type_name;
  }
  //status

  onEditStatus() {
    this.status_editing = true;
  }

  onSaveStatus() {
    const action = {
      'name': 'Смена типа',
      'old': this.oldValueType,
      'new': this.type_name,
  };
    this.cardService.updateData(this.realty.id, this.onValueType, 'status_id', 'realties', action, this.realty.user_id)
        .subscribe(
            (type) => {
              this.realty.type_name = this.type_name;
              alert(type);
            }
        );
    this.type_editing = false;
  }
  onCancelStatus(){

  }
  //Tenant

  onSaveTenant() {
    const action = {
      'name': 'Смена типа',
      'old': this.oldValueType,
      'new': this.type_name
  };
    this.cardService.updateData(this.realty.id, this.onValueType, 'type_id', 'realties', action, this.realty.user_id)
        .subscribe(
            (type) => {
              this.realty.type_name = this.type_name;
              alert(type);
            }
        );
    this.type_editing = false;
  }
  onCancelTenant() {
    this.tenant_editing = false;
  }

  onTenantEdit() {
    this.tenant_editing = true;
    this.editValueTenant = this.realty.tenant_name;

  }

  //comment
  // onCommentSave() {
  //   this.cardService.saveComment(this.realty.user_id, this.realty.realty_id, this.commentValue)
  //       .subscribe(
  //           (comment) => {
  //             console.log(comment);
  //           }
  //       );
  // }
  onSubmitComment(f: NgForm){
    this.cardService.saveComment(this.realty.user_id, this.realty.realty_id, f.value.comment)
        .subscribe(
            (comment) => {
              console.log(comment);
              f.value.comment = '';
            }
        );
  }
  removePicture() {

  }

}
