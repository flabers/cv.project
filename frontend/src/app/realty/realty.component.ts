import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {CardService} from '../card.service';

@Component({
  selector: 'app-realty',
  templateUrl: './realty.component.html',
  styleUrls: ['./realty.component.css']
})
export class RealtyComponent implements OnInit {
  id:number;
  @Input() realty;
  constructor() {
  }

  ngOnInit() {
  }
}
