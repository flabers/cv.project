import VueCard from 'vue'
import Vuex from 'vuex'

import card from "./modules/card/card";

VueCard.use(Vuex)


export default new Vuex.Store({
    modules: {
        card
    }
})