import api from '../api/api'

const state = {
    managers: [],
    loadingManagers: true,
    manager_api: {},
    manager_types: {},
    loadingTypes: true,
    create_form_display: false,
    create_button_text: "Create new",
    manager_status: false,
    create_new_manager_errors: {}
}

const getters = {
    currentManagers: state => (state.managers),
    loadingManagers: state => (state.loadingManagers),
    oneManager: state => (state.manager_api),
    getManagerTypes: state => state.manager_types,
    getLoadingTypes: state => state.loadingTypes,
    getFormDisplay: state => state.create_form_display,
    getButtonText: state => state.create_button_text,
    getManagerStatus: state => state.manager_status,
    getCreateManagerErrors: state => state.create_new_manager_errors
}

const actions = {
    resetErrorsCreateManagers({dispatch, commit}){
        commit('setCreateManagerErrors', {})
    },
    getManagers({
        dispatch,
        commit
    }) {
        api.getManagers().then((response) => {
            commit('setManagers', response.data.data)
            if (response.data.data.length > 0) {
                commit('setManagersloading')

            }

        })
    },
    setManagerStatusAction({
        dispatch,
        commit
    }, status) {
        commit('setManagerStatus', status)
    },
    getManagerApi({
        dispatch,
        commit
    }, id) {
        api.getManager(id).then((response) => {
            if (response.data.data) {
                commit('setManager', response.data.data)
            }
        })
    },
    updateManagerApi({
        dispatch,
        commit
    }, {
        id,
        data
    }) {
        api.managerUpdate(id, data).then((response) => {
            if (response.data.data) {
                commit('setManager', response.data.data)
            }
        })
    },
    setSingleManager({
        dispatch,
        commit
    }, manager) {
        commit('setManager', manager)
    },
    getManagerTypesApi({
        dispatch,
        commit
    }) {
        api.getManagerTypes().then((response) => {
            if (response.data.data) {
                commit('setManagerTypes', response.data.data)
                commit('setManagerTypesLoader', response.data.data)
            }
        })
    },
    createNewManagerApi({
        dispatch,
        commit
    }, data) {
        api.createNewManager(data).then((response) => {
            if (response.data.data) {
                commit('pushNewManager', response.data.data)
                commit('setFromDispaly', false)
            }
        }).catch((errors)=>{
            commit('setCreateManagerErrors', errors.response.data.errors["data.email"])
        })
    },
    removeManagerModule({
        dispatch,
        commit
    }, id) {
        api.removeManagerApi(id).then((response) => {
            if (response.data.data) {
                commit('setManagers', response.data.data)
                if (response.data.data.length > 0) {
                    commit('setManagersloading')
                }
            }
        })
    },
    updateFromDisplay({
        dispatch,
        commit
    }, value) {
        commit('setFromDispaly', value)
    },
    updateText({
        dispatch,
        commit
    }, value) {
        commit('setFromButtonText', value)
    },
    turnOnManagerPreloader({
        dispatch,
        commit
    }, value) {
        commit('setManagersloading', value)
    },
    setUserStatus({
            dispatch,
            commit
        },
        data
    ) {
        api.setUserStatusApi(data.id, data.status).then((response) => {
            commit('setManager', response.data.data)

        })
    }

}

const mutations = {
    setManagers(state, managers) {
        state.managers = managers
    },
    setCreateManagerErrors: (state, errors) => { 
        state.create_new_manager_errors = errors
    }
    ,
    setManagersloading(state, value = false) {
        state.loadingManagers = false
    },
    setManagerStatus(state, status) {
        state.manager_status = status
    },
    setManager(state, manager) {
        state.manager_api = manager
    },
    setManagerTypes: (state, types) => state.manager_types = types,
    setManagerTypesLoader: (state) => state.loadingTypes = false,
    pushNewManager: (state, manager) => state.managers.push(manager),
    setFromDispaly: (state, value) => state.create_form_display = value,
    setFromButtonText: (state, value) => state.create_button_text = value
}
export default {
    state,
    getters,
    mutations,
    actions
}