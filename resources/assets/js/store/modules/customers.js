import api from '../api/api'

const state = {
    customers: [],
    loadingCustomers: true
}

const getters = {
    currentCustomer: state => {
        return state.customers
    },
    loadingCustomers: state => {
        return state.loadingCustomers
    }
}

const actions = {
    getCustomers({
        dispatch,
        commit
    }) {
        api.getCustomers().then((response) => {
            commit('setCustomers', response.data.data)
            if (response.data.data.length > 0) {
                commit('setCustomersLoading')
            }
        })
    }
}

const mutations = {

    setCustomers(state, customers) {
        state.customers = customers
    },
    setCustomersLoading(state) {
        state.loadingCustomers = false
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}