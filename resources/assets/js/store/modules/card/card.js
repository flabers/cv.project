import api from '../../api/card_api'

const state = {
    object: [],
    loading_object: false,
    saving_data: false
}

const getters = {
    getObject: (state) => state.object,
    getObjectloading: (state) => state.loading_object,
    getSavingData: (state) => state.saving_data
}

const actions = {
    getObjectsApi({
        dispatch,
        commit
    }, id) {
        api.getRealtyInfo(id).then((response) => {
            commit('setObjects', response.data.data)
            commit('setLoadingObject', true)
        }).catch((error) => {
            console.log(error)
        })
    },
    updateObjectData({
        dispatch,
        commit
    }, data) {
        commit('setSavingData', true)
        api.saveData(data).then((response) => {
            commit('setObjects', response.data.data)
            commit('setSavingData', false)
        })
    },
    createNewTenant({
        dispatch,
        commit
    }, data) {
        commit('setSavingData', true)
        api.createTenant(data).then((response) => {
            commit('setObjects', response.data.data)
            commit('setSavingData', false)
        })
    }
}


const mutations = {
    setObjects: (state, object) => state.object = object,
    setLoadingObject: (state, value) => state.loading_object = value,
    setSavingData: (state, value) => state.saving_data = value
}

export default {
    state,
    getters,
    mutations,
    actions
}