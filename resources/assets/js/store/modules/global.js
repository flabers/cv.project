import api from '../api/api'

const state = {
    home_page_managers: [],
    home_page_objects: []
}

const getters = {
    currentHomePageManagers: state => {
        return state.home_page_managers
    },
    currentHomePageObjects: state => {
        return state.home_page_objects
    }
}

const actions = {
    getHomePageConclusion({
        dispatch,
        commit
    }) {
        api.getHomeConclusionApi().then((response) => {
            commit('setHomePageManagers', response.data.data.managers)
            commit('setHomePageObjects', response.data.data.objects)
        })
    }
}

const mutations = {
    setHomePageManagers(state, managers) {
        state.home_page_managers = managers
    },
    setHomePageObjects(state, objects) {
        state.home_page_objects = objects
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}