import api from '../api/api'

const state = {
    objects: [],
    loadingObjects: true
}

const getters = {
    currentObjects: state => {
        return state.objects
    },
    loadingObjects: state => (state.loadingObjects)
}

const actions = {
    getObjects({
        dispatch,
        commit
    }) {
        api.getObjects().then((response) => {
            commit('setObjects', response.data.data)
            if (response.data.data.length > 0) {
                commit('setObjectsloading')
            }
        })
    }
}

const mutations = {
    setObjects(state, objects) {
        state.objects = objects
    },
    setObjectsloading(state) {
        state.loadingObjects = false
    }
}
export default {
    state,
    getters,
    mutations,
    actions
}