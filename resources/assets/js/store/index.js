import Vue from 'vue'
import Vuex from 'vuex'

import customers from "./modules/customers";
import objects from "./modules/objects";
import managers from "./modules/managers";
import global from "./modules/global"

Vue.use(Vuex)


export default new Vuex.Store({
    modules: {
        customers,
        objects,
        managers,
        global
    }
})