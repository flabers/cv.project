export default {
    getRealtyInfo(id) {
        return new Promise((resolve, reject) => {
            axios.get('/user/api/card/' + id).then((response) => {
                resolve(response)
            })
        })
    },
    saveData(data) 
    {
        return new Promise((resolve, reject) => {
            axios.post('/user/card/update-realty', data).then((response) => {
                resolve(response)
            })
        })
    },
    createTenant(data) {
        return new Promise((resolve, reject) => {
            axios.post('/user/card/create-tenant', data).then((response) => {
                resolve(response)
            })
        })
    }
}
