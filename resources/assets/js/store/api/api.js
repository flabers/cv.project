export default {
    getCustomers() {
        return new Promise((resolve, reject) => {
            axios.post('/admin/customers').then((response) => {
                resolve(response)
            })
        })
    },
    getObjects() {
        return new Promise((resolve, reject) => {
            axios.post('/admin/objects').then((response) => {
                resolve(response)
            })
        })
    },
    getManagers() {
        return new Promise((resolve, reject) => {
            axios.post('/admin/managers').then((response) => {
                resolve(response)
            })
        })
    },
    getManager(id) {
        return new Promise((resolve, reject) => {
            axios.post('/admin/manager/' + id).then((response) => resolve(response))
        })
    },
    managerUpdate(id, data) {
        return new Promise((resolve, reject) => {
            axios.post('/admin/manager-update/' + id, {
                data
            }).then((response) => {
                resolve(response)
            })
        })
    },
    getManagerTypes() {
        return new Promise((resolve, reject) => {
            axios.post('/admin/user/types').then((response) => {
                resolve(response)
            })
        })
    },
    createNewManager(data) {
        return new Promise((resolve, reject) => {
            axios.post('/admin/manager-create', {
                data
            }).then((response) => {
                resolve(response)
            }).catch((e) =>{
                reject(e)
            })
        })
    },
    removeManagerApi(id) {
        return new Promise((resolve, reject) => {
            axios.post('/admin/remove-manager/' + id).then((response) => {
                resolve(response)
            })
        })
    },
    getHomeConclusionApi() {
        return new Promise((resolve, reject) => {
            axios.post('/admin/project/statistic').then((response) => {
                resolve(response)
            })
        })
    },
    setUserStatusApi(id, user_status) {
        return new Promise((resolve, reject) => {
            axios.post('/admin/manager-status', {
                id,
                user_status
            }).then((response) => {
                resolve(response)
            })
        })
    }

}