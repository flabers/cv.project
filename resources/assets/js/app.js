/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate';



require('./bootstrap');

window.Vue = require('vue');
Vue.config.productionTip = false;
Vue.use(VeeValidate);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import store from './store/index.js'



//import component for router
import customersComponent from './components/CustomersComponent.vue'
import managesComponent from './components/ManagersComponent.vue'
import realtyObjectaComponent from './components/RealtyObjectComponent.vue'
import mainAdminPage from './components/AdminMainPage.vue'
import managerEditPageComponent from './components/single/ManagerEditPageComponent.vue'
import PageNotFound from './components/PageNotFound.vue'


Vue.component('main-admin-page', mainAdminPage)
Vue.component('customers-component', customersComponent)
Vue.component('customer-component', require('./components/single/CustomerComponent.vue').default)
Vue.component('customer-personal-page', require('./components/CustomerPersonalPage.vue').default)
Vue.component('object-component', require('./components/single/ObjectComponent.vue').default)
Vue.component('preloader-component', require('./components/single/PreloaderComponent.vue').default)
Vue.component('manager-component', require('./components/single/ManagerComponent.vue').default)
Vue.component('manager-edit-page', managerEditPageComponent)
Vue.component('manager-create-form', require('./components/forms/CreateManagerForm.vue').default)
Vue.component('password-form-component', require('./components/forms/PasswordRestoreFormPart.vue').default)
Vue.component('types-form-component', require('./components/forms/ManagerTypesComponent.vue').default)
Vue.component('manager-status-component', require('./components/forms/ManagerStatusComponent.vue').default)
Vue.use(VueRouter)

const routes = [{
        path: '/admin/index',
        name: 'Home'
    },
    {
        path: '/admin/customers',
        component: customersComponent,
        name: 'customers'
    },
    {
        path: '/admin/managers-route',
        component: managesComponent,
        name: 'managers'
    },
    {
        path: '/admin/objects',
        component: realtyObjectaComponent,
        name: 'objects'
    },
    {
        path: '/admin/manager/:id',
        component: managerEditPageComponent,
    },
    {
        path: "*",
        component: PageNotFound
    }
]

const router = new VueRouter({
    routes,
    mode: 'history',
    linkExactActiveClass: 'active'

})

if (document.querySelector('#app') != null) {
    const app = new Vue({
        el: '#app',
        store,
        router
    });
}

//vue for card page
window.VueCard = require('vue');
VueCard.config.productionTip = false;
//validation
VueCard.use(VeeValidate);

import card_store from './store/card_index'

VueCard.component('text-input-form-component', require('./components/card_components/forms/TextInputFormComponent.vue').default);
VueCard.component('select-input-form-component', require('./components/card_components/forms/SelectInputFormComponent.vue').default);
VueCard.component('logs-component', require('./components/card_components/LogsComponent.vue').default);
VueCard.component('input-and-form-component', require('./components/card_components/forms/InputAndFormComponent.vue').default);
VueCard.component('change-status-component', require('./components/card_components/forms/ChangeStatusComponent.vue').default);
VueCard.component('object-card-component', require('./components/card_components/ObjectCardComponent.vue').default);
VueCard.component('rent-details-component', require('./components/card_components/RentDetailsComponent.vue').default);
VueCard.component('preloader-component', require('./components/single/PreloaderComponent.vue').default)
VueCard.component('log-template-component', require('./components/single/LogTemplateComponent.vue').default)


//pasport`s component

VueCard.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

VueCard.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

VueCard.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);



if (document.querySelector('#card-vue') != null) {
    const card = new VueCard({
        el: '#card-vue',
        store: card_store,
    })
}

if (document.querySelector('#card-detail-vue') != null) {
    const card = new VueCard({
        el: '#card-detail-vue',
        store: card_store,
    })
}

if (document.querySelector('#card-logs-vue') != null) {
    const card = new VueCard({
        el: '#card-logs-vue',
        store: card_store,
    })
}

if (document.querySelector('#card-status') != null) {
    const card = new VueCard({
        el: '#card-status',
        store: card_store,
    })
}