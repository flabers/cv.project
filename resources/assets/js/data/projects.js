let projects = [{
    name: 'Realty CRM',
    main_link: '/user',
    admin_panel: '/admin/index',
    avatar: '../imgs/realty.png',
    bitbucket: 'cv.project',
    main_link_text: 'User side',
    source_link: 'https://bitbucket.org/flabers/cv.project',
    description: {
        en: `This is CRM for realty bussines. This parograming desition provide easer way of control objects and manageer's actions. There is admin part of app where super viser can manage users give them permisions, remove them or make them inactive. This app in development mode so there are not all perfect but i improve it day by day`,
        ru: `Это приложения является CRM для бизнеса работающего на рынке недвижимости. Основная идея дать возможность более эффективно управлять обетами и отслеживать действия сотрудников. Также это приложения имеет админ панель через которую можно управлять работниками давать и забирать права или делать их неактивными, сменять им пароли и вовсе удалять. Приложения не идеально, но я его постоянно дорабатываю`
    }
},
{
    name: 'My cv',
    main_link: '/uploads/Full_stack_developer.pdf',
    avatar: '/imgs/cv-sized-no-pic.jpg',
    main_link_text: 'My cv',
    description: {
        en: 'This is my current cv in pdf format',
        ru: ''
    }
}
]
export {
    projects
}