import VueRouter from 'vue-router'


require('./bootstrap');

window.Vue = require('vue');



import App from './components/cv/App.vue'
import MultiLanguage from 'vue-multilanguage'
import {
    en
} from './languages/jsons/en'
import {
    ru
} from './languages/jsons/ru'



Vue.use(VueRouter)

Vue.use(MultiLanguage, {
    default: 'en',
    en: en,
    ru: ru,
})


import AboutSite from './components/cv/AboutSiteComponent.vue'
import AboutMe from './components/cv/AboutMeComponent.vue'
import Links from './components/cv/LinksComponent.vue'
import Contacts from './components/cv/ContactsComponent.vue'
import bitbucket from './components/cv/BitBucketComponent.vue'



const routes = [{
        path: '/cv/bitbucket/:project',
        component: bitbucket,
        name: '/cv/bitbucket/:project'
    },
    {
        path: '/cv/about-site',
        component: AboutSite,
        name: '/cv/about-site'
    },

    {
        path: '/cv/links',
        component: Links,
        name: '/cv/link'

    },

    {
        path: '/cv/about-me',
        component: AboutMe,
        name: '/cv/about-me'
    },
    {
        path: '/cv/contacts',
        component: Contacts,
        name: '/cv/contacts'
    }

]

const router = new VueRouter({
    routes,
    mode: 'history',
})
Vue.component('language-switch', require('./components/cv/LanguagesComponent.vue').default)
Vue.component('log-in-component', require('./components/cv/LogInComponent.vue').default)
Vue.component('project-component', require('./components/cv/ProjectComponent.vue').default)
Vue.config.productionTip = false;
new Vue({
    el: '#app',
    router,
    render: h => h(App),
})