@include('header')
<div class="container">
    <div class="row">
        @include('filter')
        <div class="col-12 col-sm-12 col-md-9 main-content">
            @foreach($realties as $realty)
                <div class="content-table">
                    <h2 class="table-header">
                        {{$realty["name"]}}
                    </h2>
                    <table class="table table-bordered mx-auto">
                        <thead>
                        <tr>
                            <td>№</td>
                            <td>Адрес</td>
                            <td>Пл-дь, м²</td>
                            <td>Тип</td>
                            <td class="hide-on-small">Арендатор</td>
                            <td>Срок аренды</td>
                            <td>Статус</td>
                        </tr>
                        </thead>
                        <tbody class="realty-content">
                        @foreach($realty["data"] as $key=>$property)
                            <tr>
                                <td>
                                    <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{++$key}}</a>
                                </td>
                                <td>
                                    <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{$property->address}}</a>
                                    @if($property->new_task) <br><span class="new-task"> <i class="fa fa-circle" aria-hidden="true"></i> Задача ожидает принятия </span> @endif
                                </td>
                                <td>
                                    <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{$property->squere}}</a>
                                </td>
                                <td>
                                    <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{$property->type_name}}</a>
                                </td>
                                <td class="hide-on-small">
                                    @can('observePermission', $property)
                                        <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{$property->tenant_name}}</a>
                                    @endcan
                                </td>
                                <td >
                                    <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{$property->end_date}}</a>
                                </td>
                                <td>
                                    <a href="{{ route('card/{id}', ['id' => $property->id]) }}">
                            <span class="status
                            @if($property->status_id == '3')
                                    red
                                @elseif($property->status_id == '2')
                                    yellow
                                @elseif($property->status_id == '1')
                                    green
                                @endif
                                    "></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endforeach
            @include('layouts.realties')
        </div>
    </div>
</div>

@include('footer')
