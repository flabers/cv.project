@include('header')
<link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <h4>
                Create new object
            </h4>
            <hr>
        </div>
    </div>
</div>
<div class="create-table container">
    <div class="row">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{route('create')}}" method="POST" class="col-12"
              enctype="multipart/form-data" id="create-form">
            <div>
                <div class="row">
                    <div class="col-xl-4">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="address">
                                Name:
                                <input class="form-control" type="text"
                                       name="realty_name"
                                       id="realty_name" placeholder="Name"
                                       value="@if(old('realty_name')){{ old('realty_name'. '') }}@endif">
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="address">
                                Address:
                                <input class="form-control" type="text"
                                       name="address"
                                       id="address" placeholder="Адрес"
                                       value="@if(old('address')){{ old('address'. '') }}@endif">
                            </label>
                        </div>

                        <div class="form-group">
                            <label for="square">
                                Area:
                                <input class="form-control"
                                       value="@if(old('square')){{ old('square'. '') }}@endif"
                                       type="number" name="square" id="square"
                                       placeholder="Area">
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="district">
                                District:
                                <select class="form-control" name="district"
                                        id="district"
                                        value="@if(old('district')){{ old('district'. '') }}@endif">
                                    @foreach($districts as $district)
                                        <option value="{{$district->district}}">{{$district->district}}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="district">
                                Number of floors:
                                <input class="form-control" type="text" name="building_floors" placeholder="Number of floors" >
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="district">
                                Floor of object:
                                <input class="form-control" type="text" placeholder="Floor of object" name="object_floor">
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="district">
                                Number of object:
                                <input class="form-control" type="text" placeholder="Number of object" name="number_object">
                            </label>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        @if(Auth::user()->user_type == 3)
                            <div class="form-group">
                                <label for="manager">
                                    Select manager:
                                    <select class="form-control" name="manager"
                                            id="manager"
                                            value="@if(old('manager')){{ old('manager'. '') }}@endif">
                                        <option value="" selected>
                                            Not selected
                                        </option>
                                        @foreach($managers as $manager)
                                            <option value="{{$manager->id}}">{{$manager->name}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                        @endif
                        <div class="form-group">
                            <label for="status">
                                Status:
                                <select class="form-control" name="status"
                                        id="status"
                                        value="@if(old('status')){{ old('status'. '') }}@endif">
                                    @foreach($statuses as $status)
                                        <option value="{{$status->id}}">{{$status->status_name}}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="type">
                                Type:
                                <select class="form-control" name="type"
                                        id="type">
                                    @foreach($property_types as $property_type)
                                        <option value="{{$property_type->id}}">{{$property_type->type_name}}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="end-date">
                                End data of the contract:
                                <input type="date" id="end-date" name="end_date"
                                       class="form-control"
                                       value="@if(old('end_date')){{ old('end_date'. '') }}@endif">
                            </label>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="tenant_checker"
                                   name="tenant_checker" value="off">

                            <div class="existing">
                                <label for="tenant">
                                    Tenant:
                                    @if(count($tenants) > 0)
                                        <select class="form-control existing-tenants"
                                                name="tenant"
                                                id="tenant">
                                            <option value="" selected>
                                                Not selected
                                            </option>
                                            @foreach($tenants as $tenant)
                                                <option value="{{$tenant->id}}">{{$tenant->tenant_name}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </label>
                            </div>
                            <div class="new-tenant d-none">
                                <label for="new-tenant">
                                    Enter first and last name:
                                    <input type="text" id="new-tenant"
                                           name="new_tenant"
                                           placeholder="Name and surname"
                                           class="form-control"
                                           value="@if(old('new_tenant')){{ old('new_tenant'. '') }}@endif">
                                </label>
                                <div class="form-group">
                                    <label for="phone">
                                        Tenant`s phone:
                                        <input type="tel" id="phone"
                                               name="phone"
                                               class="form-control"
                                               placeholder="Номер телефона"
                                               value="@if(old('phone')){{ old('phone'. '') }}@endif">
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="email-input"
                                           class="col-form-label">
                                        Tenant`s e-mail:
                                        <div class="">
                                            <input class="form-control"
                                                   type="email"
                                                   name="email"
                                                   id="mail-input"
                                                   value="@if(old('email')){{ old('email'. '') }}@endif">
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <span class="list-group-item list-group-item-action add-new-tenant">Add</span>
                        </div>
                        <div class="form-group picture-inputs">
                            <label for="file">Picture
                                <input class="form-control-file" id="file"
                                       type="file"
                                       name="file[]" multiple>
                            </label>
                        </div>
                    </div>
                    <div class="col-xl-5"></div>
                </div>
                <div class="row">
                    <div class="col-xl-2">
                        <div class="form-group">
                            <input type="submit" value="Create"
                                   class="btn btn-primary form-control">
                        </div>
                    </div>
                </div>
            </div>


        </form>


    </div>
</div>

<script src="{{asset('js/create_from.js')}}?{{config('custom_config.script_version')}}"></script>
@include('footer')