@include('header')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>
                {{$user_name}}
            </h2>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <td>№</td>
                    <td>Адрес</td>
                    <td>Пл-дь, м²</td>
                    <td>Тип</td>
                    <td>Арендатор</td>
                    <td>Срок аренды</td>
                    <td>Статус</td>
                </tr>
            </thead>
            @foreach($realties as $key => $realty)
                        <tr>
                            <td><a href="{{ route('card/{id}', ['id' => $realty->id]) }}">{{++$key}}</a></td>
                            <td><a href="{{ route('card/{id}', ['id' => $realty->id]) }}">{{$realty->address}}</a></td>
                            <td><a href="{{ route('card/{id}', ['id' => $realty->id]) }}">{{$realty->squere}}</a></td>
                            <td><a href="{{ route('card/{id}', ['id' => $realty->id]) }}">{{$realty->type_name}}</a></td>
                            <td><a href="{{ route('card/{id}', ['id' => $realty->id]) }}">{{$realty->tenant_name}}</a></td>
                            <td><a href="{{ route('card/{id}', ['id' => $realty->id]) }}">{{$realty->end_rend}}</a></td>
                            <td>
                                <a href="">
                            <span class="status
                            @if($realty->status_id == '3')
                                    red
                                @elseif($realty->status_id == '2')
                                    yellow
                                @elseif($realty->status_id == '1')
                                    green
                                @endif
                                    "></span>
                                </a>
                            </td>
                        </tr>
            @endforeach
            </table>
        </div>
        </div>
    </div>
</div>
@include('footer')