@include('header')
<link rel="stylesheet" href="{{asset('css/jquery.fancybox.min.css')}}">
<link rel="stylesheet" href="{{asset('css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('css/Sortable/jquery-ui.theme.min.css')}}">
<link href="{{ asset('css/app_card.css') }}" rel="stylesheet">
<div class="container">
    <span class="d-none realty_id_vue">{{$realty->id}}</span>
    <h2 class="main-name">{{$realty->realty_name}}</h2>
    <div class="row">
        <div class="col-xl-3 col-xs-12 slider-wrap mx-auto">
            <div class="swiper-container gallery-top">
                <div class="swiper-wrapper">
                    @foreach($realty->pictures as $picture)
                        <div class="swiper-slide">
                            <a href="{{asset('uploads/'.$picture->picture_name)}}"
                               data-fancybox="group"
                               data-caption="Caption #{{$loop->index + 1}}">
                                <img src="{{asset('uploads/'.$picture->picture_name)}}">
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next swiper-button-white"></div>
                <div class="swiper-button-prev swiper-button-white"></div>
            </div>
            <div class="small-pictures">
                <div class="">
                    @foreach($realty->pictures as $picture)
                        <a href="{{asset('uploads/'.$picture->picture_name)}}"
                           data-fancybox="group"
                           data-caption="Caption #{{$loop->index + 1}}"
                           class="picture-wrap">
                            <img src="{{asset('uploads/'.$picture->picture_name)}}">
                            @can('makeAction', $realty)
                                <i class="fa fa-times delete-picture"
                                   data-id="{{$picture->id}}"
                                   aria-hidden="true"></i>
                            @endcan
                        </a>
                    @endforeach
                </div>
            </div>
            @can('makeAction', $realty)
                <p><a href="{{ route('picture/{id}', ['id' => $realty->id]) }}"
                      class="btn btn-primary">Upload picture</a></p>
            @endcan
            @can('observePermission', $realty)
                <p><a class="pdf-link btn btn-outline-success"
                      href="{{route('pdf/{id}', $realty->id)}}">Download pdf
                        version</a></p>
            @endcan
            <div class="picture-sort">
                @can('makeAction', $realty)
                    <h4>Picture sort</h4>
                    <div>
                        <div id="sortable">
                            @foreach($realty->pictures as $key => $picture)
                                <div class="ui-state-default picture-block"
                                     data-id="{{$picture->id}}">
                                    <div class="sort-picture d-flex">
                                        <img src="{{asset('uploads/'.$picture->picture_name)}}">
                                        <div class="sort-details-wrap">
                                            <span class="sort-picture-name">{{$picture->picture_name}}</span><br>
                                            <span class="ui-icon ui-icon-arrowthick-2-n-s sort-value">{{$picture->sort}}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button data-id="{{$realty->id}}" class="btn btn-primary save-sort">Save
                        </button>
                    </div>
                @endcan
            </div>
        </div>
        <div class="col-xl-9 col-xs-12">
            @include('layouts.tasks')
            <div>
                <div class="row details-block">
                    <div class="col-xl-3 col-6 col-sm-6 block-characteristics ">
                        <p>Square</p>
                        <p>Address</p>
                        <p>District</p>
                        <p>Type</p>
                        <p>Number of floors</p>
                        <p>Floor of object</p>
                        <p>Number of object</p>
                    </div>
                    <div class="col-xl-4 col-6 col-sm-6">
                        <div id="card-vue">
                            <object-card-component></object-card-component>
                        </div>
                    </div>    
                    <div class="col-xl-4 col-6 col-sm-6">      
                        <div id="card-status">
                            <change-status-component></change-status-component>
                        </div>
                    </div>
                </div>


                <div class="row details-block">
                    <div class="col-xl-3 col-6 col-sm-6 block-characteristics">
                        <p>Manager</p>
                        <p>Tenant</p>
                        <p>Term of the contract</p>
                        <p>Tenant`s phone</p>
                        <p>Tenant`s email</p>
                    </div>
                    <div class="col-xl-5 col-6 col-sm-6 mr-auto">
                        <div id="card-detail-vue">
                            <rent-details-component></rent-retails-component>
                        </div>
                    </div>
                    <div class="col-xl-4"></div>
                </div>
            </div>
            <div>
                <div class="row">
                    <div class="col-xl-12">
                        <hr>
                        <div class="row">
                            <div class="col-xl-10">
                                <h3>Object`s tasks</h3>
                                <h5 class="text-danger task-header hide-headers header-task-waiting">Waiting for acceptance</h5>
                            </div>
                            <div class="col-xl-2">
                                {{--check enginner--}}
                                <p>
                                    <a href="#"
                                       class="open-task-wrap btn btn-outline-info">Tasks archive</a>
                                </p>
                            </div>
                        </div>
                        <div class="task-block">
                            <div class="container">
                                @if(count($tasks) > 0)
                                    @foreach($tasks as $task)
                                        @if($task->status != "New")
                                            @continue
                                        @endif
                                        <div class="row">
                                            <div class="col-xl-7 task-text">
                                                <p class="">{{ $task->task }}</p>
                                            </div>
                                            <div class="col-xl-3">
                                                @if(Auth::user()->user_type == 4)
                                                    <input type="date"
                                                           class="form-control task-end-date"
                                                           required>
                                                @endif
                                            </div>
                                            <div class="col-xl-2">
                                                @if(Auth::user()->user_type == 4)
                                                    <button class="btn btn-primary accept-task"
                                                            data-taskid="{{$task->id}}">
                                                        <span class="select-word">Select</span>
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <h5 class="text-primary tasks-in-process @if(count($tasks) == 0 ) hide-headers @endif">
                            In process</h5>
                        <div class="container">
                            <div class="task-in-process-card">
                                @if(count($tasks) > 0)
                                    @foreach($tasks as $task)
                                        @if($task->status != "In process")
                                            @continue
                                        @endif
                                        <div class="row">
                                            <div class="col-xl-10">
                                                <p>
                                                    <strong>{{$task->name}}</strong>
                                                </p>
                                                <p>Data of completion: {{formatDateOnly($task->complete_engineer)}}</p>
                                                <p>{{$task->task}}</p>
                                            </div>
                                            <div class="col-xl-2 task-in-process-btns">
                                                @if (Auth::user()->user_type != 4)
                                                    <button class="btn btn-success end-task-card"
                                                            data-taskid="{{$task->id}}">
                                                        Completed
                                                    </button>
                                                    <button type="button"
                                                            class="btn btn-dark remove-task-card"
                                                            data-taskId="{{$task->id}}">
                                                        Cancel
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        @can('makeAction', $realty)
                            <div class="row">
                                <div class="col-xl-10">
                                    <h5 class="text-light new-task-header">New task</h5>
                                    <form id="new-task-engineer-card"
                                          class="form">
                                        <div class="form-group">
                                            <textarea rows="7" cols="40"
                                                      id="new-task-form"
                                                      type="textaerea"
                                                      name="new_task"
                                                      class="form-control"
                                                      placeholder="New task"></textarea>
                                        </div>
                                        <input type="hidden" name="realty_id"
                                               value="{{$realty->id}}">
                                        <input type="hidden" name="user_id"
                                               value="{{Auth::user()->id}}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <button class="btn btn-primary">
                                                Add
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xl-2"></div>
                            </div>
                        @endcan

                        <hr>
                        @can('makeAction', $realty)
                            <div id="card-logs-vue">
                                <logs-component></logs-component>
                            </div>
                        @endcan
                            <hr>

                            <h3 class="comments-header">
                                Comments</h3>
                            <div class="comments">
                                @if( $comments != false AND count($comments) > 0)
                                    @foreach($comments as $comment)
                                        <div class="container">
                                            <div class="row history-line">
                                                <div class="col-xl-3">
                                                    <p class="font-weight-bold">{{$comment->name}}</p>
                                                    <div class="history-date"><span
                                                                class="sub-info-log">{{$comment->position}}</span>
                                                    </div>
                                                    <div class="history-position">
                                                        <span class="sub-info-log">{{ date('H:i d.m.Y', strtotime($comment->created_at))}}</span>
                                                    </div>
                                                </div>
                                                <div class="col-xl-9 col-offset-4">
                                                    <p>{{ $comment->comment }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            @can('makeAction', $realty)
                                <h5 class="text-light new-task-header">Add comment</h5>
                                <form action="" class="comment-form">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="hidden" name="realty_id"
                                               value="{{$realty->id}}">
                                        <input type="hidden" name="user_id"
                                               value="{{$realty->user_id}}">
                                        <textarea rows="3" name="comment"
                                                  class="form-control"
                                                  placeholder="Your comment..."></textarea>
                                        <button type="submit"
                                                class="btn btn-primary">
                                            Add
                                        </button>
                                    </div>
                                </form>
                            @endcan
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.picture_modal')

    <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{asset('js/card.js')}}?{{config('custom_config.script_version')}}"></script>
        <script src="{{asset('js/jquery.fancybox.min.js')}}"></script>
        <script src="{{asset('js/engineer.js')}}?{{config('custom_config.script_version')}}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/js/swiper.min.js"></script>
    </div>
</div>

@include('footer')