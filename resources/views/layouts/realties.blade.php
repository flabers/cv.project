<div class="content-table">
    @if(count($single_realties) > 0)
        <h2 class="table-header">
            Объекты без управляющего
        </h2>
        <table class="table table-bordered">
            <thead>
            <tr>
                <td>№</td>
                <td>Адрес</td>
                <td>Пл-дь, м²</td>
                <td>Тип</td>
                <td>Арендатор</td>
                <td>Срок аренды</td>
                <td>Статус</td>
            </tr>
            </thead>
            <tbody class="realty-content">
            @foreach($single_realties as $key=>$property)
                <tr>
                    <td>
                        <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{++$key}}</a>
                    </td>
                    <td>
                        <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{$property->address}}</a>
                        @if($property->new_task) <br><span class="new-task"> <i
                                    class="fa fa-circle" aria-hidden="true"></i> Задача ожидает принятия </span> @endif
                    </td>
                    <td>
                        <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{$property->squere}}</a>
                    </td>
                    <td>
                        <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{$property->type_name}}</a>
                    </td>
                    <td>
                        @if(Auth::user()->user_type == 1 || Auth::user()->user_type == 3 || (Auth::user()->user_type == 2 && Auth::user()->id == $property->user_id))
                            <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{$property->tenant_name}}</a>
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('card/{id}', ['id' => $property->id]) }}">{{$property->end_date}}</a>
                    </td>
                    <td>
                        <a href="{{ route('card/{id}', ['id' => $property->id]) }}">
            <span class="status
            @if($property->status_id == '3')
                    red
                @elseif($property->status_id == '2')
                    yellow
                @elseif($property->status_id == '1')
                    green
                @endif
                    "></span>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
</div>