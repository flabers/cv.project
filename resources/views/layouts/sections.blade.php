@section('tenant')
    <div>
        <a href="#" class="show-eidt-block">
            <img src="{{asset('imgs/u434.png')}}" alt="edit">
        </a>
        <span class="old-tenant-value">{{$realty->tenant_name}}</span>
        {{--tenant edit modal window--}}
        <div class="modal input-edit-block" tabindex="-1"
             role="dialog">
            <div class="modal-dialog"
                 role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Измения
                            орендатора: </h5>
                    </div>
                    <div class="modal-body">
                        <div id="accordion" role="tablist">
                            <div class="card">
                                <div class="card-header"
                                     role="tab"
                                     id="headingOne">
                                    <h5 class="mb-0">
                                        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Выбрать
                                            сушествуюшего
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class="existing_tenant">
                                            <select name=""
                                                    id=""
                                                    class="form-control edit-tenant-input active-input">
                                                @foreach($tenants as $tenant)
                                                    <option value="{{$tenant->id}}"
                                                            @if($realty->tenant_id == $tenant->id) selected @endif >
                                                        {{$tenant->tenant_name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card ">
                                <div class="card-header"
                                     role="tab"
                                     id="headingTwo">
                                    <h5 class="mb-0">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Добавить нового
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p class="new_tenant">
                                        <form id="new-user-form"
                                              class="edit-tenant-input"
                                              method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden"
                                                   value="{{Auth::user()->id}}"
                                                   name="user_id">
                                            <input type="hidden"
                                                   value="{{$realty->id}}"
                                                   name="realty_id">
                                            <div class="form-group">
                                                <input class="form-control"
                                                       type="text"
                                                       class="new-user-phone"
                                                       name="phone"
                                                       placeholder="Телефон">
                                                <input class="form-control"
                                                       type="text"
                                                       name="email"
                                                       placeholder="Email">
                                                <input class="form-control"
                                                       type="text"
                                                       name="tenant_name"
                                                       placeholder="Имя и фамилия орендатора">
                                                <input class="form-control"
                                                       type="date"
                                                       name="end_date">
                                            </div>
                                            {{--<input type="submit" value="Сохранить" class="btn btn-primary">--}}
                                        </form>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-primary tenant-edit"
                                data-id="{{$realty->id}}"
                                data-user="{{Auth::user()->id}}">
                            Сохранить
                        </button>
                        <button type="button"
                                class="btn btn-secondary hide-edit-block"
                                data-dismiss="modal">Отменить
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@show