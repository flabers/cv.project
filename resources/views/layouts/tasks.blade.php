{{--temp--}}
<div class="modal tasks-wrap" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Список задач</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="close-modal-window">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <div id="accordion_task" role="tablist">
                        @if(count($tasks))
                        @foreach($tasks as $task)
                            <div class="card">
                                <div class="card-header" role="tab" id="heading_{{$task->id}}">
                                    <h5 class="mb-0">
                                        <a data-toggle="collapse" href="#collapse_{{$task->id}}" aria-expanded="true" aria-controls="collapse_{{$task->id}}">
                                            {{$task->task}}
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse_{{$task->id}}" class="collapse @if($loop->first) show @endif" role="tabpanel" aria-labelledby="heading_{{$task->id}}" data-parent="#accordion_task">
                                    <div class="card-body">
                                        <div class="jumbotron">
                                            <h1 class="display-5">{{$task->task}}</h1>
                                            <p class="lead">Задачу поставил: {{$task->name}}</p>
                                            <p class="lead">Задача поставлена: {{formatDateOnly($task->start)}}</p>
                                            <p class="lead">Должна быть выполена: {{formatDateOnly($task->complete)}}</p>
                                            <p class="lead task-status">Текуший статус: {{$task->status}}</p>
                                            @if($task->complete_engineer)
                                                <p class="lead complete_engineer-data">Конечная дата назначеная инженером: <br>{{$task->complete_engineer}}</p>
                                            @endif
                                            <hr class="my-4">
                                            <p class="lead">
                                                @if(Auth::user()->user_type == 4)
                                                    <label for="set-new-date-{{$task->id}}">Назначить новую конечную дату <input type="checkbox" id="set-new-date-{{$task->id}}" class="show-engineer-date"></label>
                                                    <input type="date" class="complete_engineer form-control" disabled >
                                                    <a class="btn btn-primary btn-lg process" href="#" role="button" data-id="{{$task->id}}">Оброботка</a>
                                                    <a class="btn btn-primary btn-lg close-task" href="#" role="button" data-id="{{$task->id}}">Закрыть</a>
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @endif
                            <div class="card">
                                <div class="card-header" role="tab" id="heading_new">
                                    <h5 class="mb-0">
                                        <a data-toggle="collapse" href="#collapse_new" aria-expanded="true" aria-controls="collapse_new">
                                            Новая Задача
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse_new" class="collapse" role="tabpanel" aria-labelledby="heading_new" data-parent="#accordion_task">
                                    <div class="card-body">
                                        <form id="new-task-engineer" class="form">
                                            <div class="form-group">
                                                <label for="new-task">Новая задача:</label>
                                                <textarea rows="7" cols="40" id="new-task" type="textaerea" name="new_task" class="form-control"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="complete">Введите конечную дату:</label>
                                                <input id="complete" type="date" name="complete" class="form-control">
                                            </div>

                                            <input type="hidden" name="realty_id" value="{{$realty->id}}">
                                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                            {{ csrf_field() }}

                                        </form>
                                    </div>
                                </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" form="new-task-engineer" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary close-modal-window" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>
{{--temp--}}