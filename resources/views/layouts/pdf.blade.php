<div class="container">

    <h3>Информация об объекте {{$realty->realty_name}}</h3>
    <hr>
    <table>
        @if($realty->squere)
            <tr>
                <td>Площадь:</td>
                <td>{{$realty->squere}} м<sup>2</sup></td>
            </tr>
        @endif
        @if($realty->address)
            <tr>
                <td>Адрес:</td>
                <td>{{$realty->address}}</td>
            </tr>
        @endif
        @if($realty->district)
            <tr>
                <td>Район:</td>
                <td>{{$realty->district}}</td>
            </tr>
        @endif
        @if($realty->type_name)
            <tr>
                <td>Тип:</td>
                <td>{{$realty->type_name}}</td>
            </tr>
        @endif
        @if($realty->building_floors)
            <tr>
                <td>Этажность здания:</td>
                <td>{{$realty->building_floors}}</td>
            </tr>
        @endif
        @if($realty->object_floor)
            <tr>
                <td>Этаж объекта:</td>
                <td>{{$realty->object_floor}}</td>
            </tr>
        @endif
        @if($realty->number_object)
            <tr>
                <td>Номер объекта:</td>
                <td>{{$realty->number_object}}</td>
            </tr>
        @endif
    </table>
    <hr>
    <table style="width: 730px">
        @foreach($realty_pictures as $key => $picture)
            <tr style="">
                <td>
                    <img style="width: 350px"
                         src="{{asset('uploads/'.$picture[0]->picture_name)}}">
                </td>
                @if(isset($picture[1]->picture_name))
                    <td>
                        <img style="width: 350px"
                             src="{{asset('uploads/'.$picture[1]->picture_name)}}">
                    </td>
                @else
                    <td></td>
                @endif
            </tr>
        @endforeach
    </table>

</div>
</body>
</html>