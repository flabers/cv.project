@include('header')
<div class="container">
    <span class="d-none input-counter">0</span>
    <span class="d-none realty_id">{{$realty_id}}</span>
    <div class="row">
        <div class="col-12">
            <h2>Form for upload pictures:</h2>
            {!! Form::open(array('action' => 'PictureController@store','enctype' => 'multipart/form-data', 'method' => 'post', 'id'=>'upload-form')) !!}
            <div class="form-group picture-inputs">
                <input type="hidden" value="{{$realty_id}}" name="realty_id" >
                <label for="file">Pictures
                    <input class="form-control-file" id="file" type="file"
                           name="file[]" multiple>
                </label>
            </div>
            <input type="submit" class="btn btn-primary" value="Upload">
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script src="{{asset('js/upload.js')}}?{{config('custom_config.script_version')}}"></script>
@include('footer')