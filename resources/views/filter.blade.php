<span class="d-block d-md-none filter-button-mobile p-1 border"><i class="fas fa-filter"></i></span>
<div class="col-12 col-sm-3 cl-md-3 col-lg-3 col-xl-3 filter-sidebar d-none d-md-block">
    <aside class="side-bar">
        <form action="" class="filter-form">
            {{ csrf_field() }}
            <div class="filter form-group">
                <div>
                    <p><strong>Managers</strong></p>
                    @foreach($users as $user)
                        @if($user->user_type != 2)
                            @continue
                        @endif
                        <div>
                            <label for="manager_{{$user->id}}"> 
                                <input class=""
                                    id="manager_{{$user->id}}"
                                    type="checkbox"
                                    name="manager_{{$user->id}}"> {{$user->name}}
                            </label>
                        </div>
                    @endforeach
                </div>
                <hr>
                <div>
                    <p><strong>Object status</strong></p>
                    @foreach($statuses as $status)
                        <div>
                            <label for="status_{{$status->id}}"><input class=""
                                                                       id="status_{{$status->id}}"
                                                                       type="checkbox"
                                                                       name="status_{{$status->id}}"> {{$status->status_name}}
                            </label>
                        </div>
                    @endforeach
                </div>
                <hr>
                <div>
                    <p><strong>Square, м²</strong></p>

                    <div class="row">
                        <div class="col-xl-4"><input type="number"
                                                     class="filter-square-input-from form-control filter-square-input"
                                                     name="square_from"></div>
                        <div class="col-xl-1">-</div>
                        <div class="col-xl-4"><input type="number"
                                                     name="square_to"
                                                     class="filter-square-input-to form-control filter-square-input">
                        </div>
                        <div class="col-xl-2"><span
                                    class="filter-by-square btn btn-primary">Ok</span>
                        </div>
                    </div>

                </div>
                <hr>
                <div>
                    <p><strong>City disctirct</strong></p>
                    @foreach($district as $dist)
                        <div class="from-group">
                            <label for="district_{{$dist->district}}"><input
                                        class=""
                                        id="district_{{$dist->district}}"
                                        type="checkbox"
                                        name="district_{{$dist->district}}"> {{$dist->district}}
                            </label>
                        </div>
                    @endforeach
                </div>
                <hr>
                <div>
                    <p><strong>End of contract</strong></p>

                    <div class="row">
                        <div class="col-xl-8">
                            <select class="form-control" id='gMonth2'>
                                <option selected value=''>Month</option>
                                <option value='1'>January</option>
                                <option value='2'>February</option>
                                <option value='3'>March</option>
                                <option value='4'>April</option>
                                <option value='5'>May</option>
                                <option value='6'>June</option>
                                <option value='7'>July</option>
                                <option value='8'>August</option>
                                <option value='9'>September</option>
                                <option value='10'>October</option>
                                <option value='11'>November</option>
                                <option value='12'>December</option>
                            </select>
                        </div>
                        <div class="col-xl-4">
                            <span class="btn btn-primary filter-by-mouth">Ok</span>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-xl-6">
                        <input type="submit"
                               class="form-control btn btn-primary"
                               value="Применить">
                    </div>
                    <div class="col-xl-6">
                        {{--<span type="" class="form-control btn btn-info remove-filter">Cencel</span>--}}
                    </div>
                </div>

        </form>
        <div class="statistics">
            <p><strong>Statistics</strong></p>
            <div>
                <p>Total number of objects: {{$total}}<br/>
                    Rented: {{$rented}}<br/>
                    Unreliable: {{$not_reliable}}<br/>
                    Free: {{$not_rented}}</p>
            </div>
        </div>
    </aside>
    <script src="{{asset('js/filter.js')}}?{{config('custom_config.script_version')}}"></script>
</div>