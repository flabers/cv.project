<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Liberty</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}?{{config('custom_config.script_version')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/css/swiper.min.css">
    <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="/favicon.ico"/>
</head>
<body>
<div class="">
    <header>
        <div class="container">
        <div class="row d-md-flex justify-content-between">
            <div class="col-xl-3 col-sm-12 col-md-4 col-lg-2 d-flex justify-content-center">
                <div class="user-wrap">
                    @guest
                    <a href="{{ route('login') }}">Log in</a>
                    @else
                        <p class="dropdown header-navigate-buttons">
                            <a title="{{ Auth::user()->name }}" class="btn btn-success" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Log out
                            </a>
                            @if (Route::getCurrentRoute()->uri != 'user')
                                <a class="home-link btn btn-success" href="{{route('/')}}"><i
                                class="fa fa-long-arrow-left"
                                aria-hidden="true"></i> All objects</a>
                            @endif 
                        <form id="logout-form" action="{{ route('logout') }}"
                              method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        </p>
                        @endguest
                </div>               
            </div>
            <div class="col-xl-5  col-sm-12 col-md-4 col-lg-6">
                <div>
                    <form class="form-inline my-2 my-lg-0 d-flex justify-content-center">
                        <input class="form-control mr-sm-2 w-75 w-md-100" type="search"
                               placeholder="Search" aria-label="Поиск"
                               id="live-search">
                    </form>
                    <div class="search-result-popup">
                        
                    </div>
                </div>

            </div>
            <div class="col-xl-4  col-sm-12 col-md-4 col-lg-4 header-rigth-part">
                @can('permission', Auth::user())
                    <a class="btn btn-success btn-create-object m-1 header-buttons"
                    href="{{route('create')}}">Add new <span class="sr-only">(current)</span></a>
                @endcan
                @can('adminPanel', Auth::user())
                    <a class="btn btn-success btn-create-object m-1 header-buttons"
                    href="/admin/index">Admin panel<span class="sr-only">(current)</span></a>
                @endcan
            </div>
        </div>
        </div>
    </header>
</div>

<hr>

