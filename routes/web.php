<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\User;
use App\Models\UserType;
use App\Models\Realty;



Auth::routes();



Route::get('auth/github', 'Auth\LoginController@redirectToProvider')->name('github');
Route::get('auth/github/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('auth/google', 'Auth\LoginController@redirectToProviderGoogle')->name('google');
Route::get('auth/google/callback', 'Auth\LoginController@handleProviderCallbackGoogle');

Route::get('log-out', function() {
    Auth::logout();
    return response()->json([
        'result' => true
    ], 200);
});

//test section

Route::get('upload', function() {
    return view('images');
});
Route::post('upload', function() {
    return response()->json([
        'name' => 'lox'
    ], 200);
});

Route::resource('task', 'TaskController')->only('store', 'destroy', 'update');

Route::get('bitbucket', 'Api\\BitBucketController@getSource');

// cv section

Route::group(['prefix' => 'cv'], function(){
    Route::get('{any}', function() {
        return view('cv.index');
    });
});

Route::get('/', function () {
    return redirect('cv/about-me');
});



// managers side routs



Route::group(['prefix' => 'user', 'middleware' => ['web', 'auth']], function () {

    Route::get('create', [
        'uses' => 'CreateController@index',
        'as' => 'create',
        'middleware' => ['create_prevent']
    ]);
    Route::post('create', [
        'uses' => 'CreateController@save',
        'as' => 'create',
        'middleware' => ['create_prevent']
    ]);

    Route::get('/', [
        'as' => '/',
        'uses' => 'RealtyController@index'
    ]);
    Route::post('/filter', [
        'uses' => 'RealtyController@filter',
        'as' => 'filter'
    ]);

    Route::get('home', [
        'uses' => 'RealtyController@index'
    ]);
    Route::get('home/by-status', [
        'uses' => 'RealtyController@sortRealtyByStatus'
    ]);
    Route::get('card/{id}', [
        'as' => 'card/{id}',
        'uses' => 'RealtyController@getCardRealty'
    ]);
    Route::get('api/card/{id}', [
        'as' => 'api/card/{id}',
        'uses' => 'Api\\RealtyController@getCardRealty'
    ]);
    Route::post('card/create-task', [
        'uses' => 'EngineerController@createTask'
    ]);
    Route::post('/', [
        'uses' => 'UserController@signup',
    ]);
    Route::post('/signin', [
        'uses' => 'UserController@signin'
    ]);
	Route::post('/card/create-tenant', [
		'uses' => 'RealtyController@createTenant'
	]);
    Route::post('/filter-month', [
        'uses' => 'RealtyController@getRealtyByMonth'
    ]);
    Route::post('/filter-square', [
        'uses' => 'RealtyController@filterBySquare'
    ]);
    Route::post('/search', [
        'uses' => 'RealtyController@liveSearch'
    ]);
    Route::post('/user/search', [
        'uses' => 'RealtyController@liveSearch'
    ]);
    Route::post('card/user/search', [
        'uses' => 'RealtyController@liveSearch'
    ]);
    Route::post('picture/user/search', [
        'uses' => 'RealtyController@liveSearch'
    ]);
    Route::post('/card/completed-task', [
        'uses' => 'EngineerController@completed'
    ]);
    Route::post('card/comment', [
        'uses' => 'RealtyController@insertComment'
    ]);
    Route::post('card/update-realty', [
        'uses' => 'RealtyController@updateRealty'
    ]);
    Route::post('/user', [
        'uses' => 'UserController@getActiveUser'
    ]);
    Route::post('/picture', [
        'uses' => 'PictureController@store'
    ]);
    Route::get('/user-details/{id}', [
        'as' => 'user-details/{id}',
        'uses' => 'RealtyController@userDetails'
    ]);
    Route::get('picture/{id}',
        [
            'as' => 'picture/{id}',
            'uses' => 'PictureController@index'
        ]
    );
    Route::post('picture',
        [
            'as' => 'picture',
            'uses' => 'PictureController@store'
        ]
    );
//    engineer block
    Route::post('/card/close-task', [
        'uses' => 'EngineerController@closeTask'
    ]);
    Route::post('/card/process-task', [
        'uses' => 'EngineerController@processTask'
    ]);
    Route::post('/card/remove-picture', [
        'uses' => 'PictureController@removePicture',
    ]);
    Route::get('card/remove-object/{id}', [
        'uses' => 'RealtyController@removeObject'
    ]);
    Route::post('card/sort/{id}', [
        'uses' => 'PictureController@updateSortPicture'
    ]);
    Route::get('pdf/{id}', [
        'uses' => 'PdfController@export',
        'as' => 'pdf/{id}'
    ]);
    Route::get('php-info', function (){
        return phpinfo();
    });
});



// admin panel routs
Route::get('admin', function(){
    return redirect('admin/index');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Panel', 'middleware' => ['web', 'auth']], function () {
    Route::get('{any}', function(){
        return view('admin.admin');
    })->name('admin/index');

    Route::post('get-home-page-link', function(){
        return response()->json([
            'link' => route('/user')
        ], 200);
    });
    Route::post('/customers', 'CustomersController@index');
    Route::post('/objects', 'ObjectController@objects');
    Route::post('/managers', 'ManagersController@show');
    Route::post('/manager-status', 'ManagersController@setStatus');
    Route::post('/manager/{id}', 'ManagersController@manager');
    Route::post('/user/types', 'ManagersController@userTypes');
    Route::post('remove-manager/{id}', 'ManagersController@remove');
    Route::post('/manager-update/{id}', 'ManagersController@update');
    Route::post('/manager-create', 'ManagersController@create');
    Route::post('/project/statistic', 'HomeController@conclusion');
});