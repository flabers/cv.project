<?php

use Illuminate\Database\Seeder;
use UserTypeTableSeeder;
use UserTypeSeed;
use UserTypeTableSeede;
use RealtyTypeSeede;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(UserTypeSeed::class);
        $this->call(UserTypeTableSeeder::class);
        $this->call(RealtyTypeSeeder::class);
    }
}
