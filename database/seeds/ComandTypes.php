<?php

use Illuminate\Database\Seeder;
use App\Action;
class ComandTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions = [
            'Объект добавлен',
            'Просмотрел объект',
            'Сменил статус на: ',
            'Редактировала: '
        ];
        function insertInDb($str)
        {
            Action::create([
                'action_name' => $str
            ]);
            return true;
        }
        array_map('insertInDb', $actions);
    }
}
