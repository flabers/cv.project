<?php

use Illuminate\Database\Seeder;
use App\Models\District;
class RealkGDistricts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions = [
            'Down town',
            'East side',
            'West side',
        ];
        function insertInDb($str)
        {
            District::create([
                'district_name' => $str,
            ]);
            return true;
        }
        array_map('insertInDb', $actions);
    }
}
