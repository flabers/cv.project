<?php

use Illuminate\Database\Seeder;
use App\Models\UserType;

class UserTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions = [
            'Редактор',
            'Менеджер',
            'Палпатин',
            'Бог'
        ];
        function insertInDb($str)
        {
            UserType::create([
                'position' => $str,
            ]);
            return true;
        }
        array_map('insertInDb', $actions);
    }
}
