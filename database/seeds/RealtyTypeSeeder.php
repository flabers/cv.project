<?php

use Illuminate\Database\Seeder;
use App\Models\PropertyType;
class RealtyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$actions = [
			'Shopping mall',
			'Supermarket',
		];
		function insertInDb($str)
		{
			PropertyType::create([
				'type_name' => $str,
			]);
			return true;
		}
		array_map('insertInDb', $actions);
    }
}
