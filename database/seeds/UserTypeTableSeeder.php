<?php

use Illuminate\Database\Seeder;
use App\Models\UserType;
class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions = [
            'Владелец',
            'Управляющий',
            'Редактор',
            'Инженер',
            'Менеджер'
        ];
        function insertInDb($str)
        {
            UserType::create([
                'position' => $str
            ]);
            return true;
        }
        
        array_map('insertInDb', $actions);

        UserType::create([
            'position' => 'Customer',
            'id' => 9
        ]);
    }
}
