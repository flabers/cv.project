<?php

use Illuminate\Database\Seeder;
use App\Realty;
class RealtiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();
        
        for ($i = 0; $i < 40; $i++) {
            Realty::create([
                'status_id' => rand(0,3),
                'type_id' => rand(0,3),
                'user_id' => rand(0,5),
                'address' => $faker->address,
                'district' => str_random(10),
                'squere' => rand(40,155),
            ]);
        }
    }
}
