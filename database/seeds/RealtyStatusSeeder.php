<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class RealtyStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            'Free',
            'Rented',
        ];
        function insertInDb($str)
        {
            Status::create([
                'status_name' => $str,
            ]);
            return true;
        }
        array_map('insertInDb', $statuses);
    }
}
