<?php

use Illuminate\Database\Seeder;
use App\Tenant;
class TenantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();
        
        for ($i = 0; $i < 30; $i++) {
            Tenant::create([
                'tenant_name' => $faker->name,
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,
                'start_rend' => $faker->date,
                'end_rend' => $faker->date,
                'realty_id' => rand(1, 40)
            ]);
        }
    }
}
