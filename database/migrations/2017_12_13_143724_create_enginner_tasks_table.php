<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnginnerTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('engineer_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task', 255);
            $table->date('complete_engineer');
            $table->date('complete');
            $table->string('status', 255);
            $table->integer('realty_id')->nullable($value = true)->unsigned();
            $table->integer('user_id')->nullable($value = true)->unsigned();
            $table->integer('engineer_id')->nullable($value = true)->unsigned();
            $table->date('start');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('engineer_tasks');
    }
}
