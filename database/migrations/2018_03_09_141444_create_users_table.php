<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('email', 255);
            $table->string('password', 255);
            $table->integer('user_type')->nullable($value = true);
            $table->integer('user_status')->nullable()->default(0);
            $table->integer('github_id')->nullable($value = true);
            $table->integer('google_id')->nullable($value = true);
            $table->string('remember_token', 100)->nullable($value = true);
            $table->string('github_avatar', 255)->nullable($value = true);
            $table->string('google_avatar', 255)->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
