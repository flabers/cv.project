<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealtiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realties', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('status_id');
            $table->string('address', 255);
            $table->string('realty_name', 255)->nullable(true);
            $table->string('building_floors', 255)->nullable(true);
            $table->string('object_floor', 255)->nullable(true);
            $table->string('number_object', 255)->nullable(true);
            $table->string('district');
            $table->integer('user_id')->nullable(true);
            $table->double('squere', 15, 2);
            $table->date('end_date')->nullable(true);
            $table->date('start_date')->nullable(true);
            $table->integer('type_id');
            $table->integer('tenant_id')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realties');
    }
}
