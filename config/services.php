<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => 'sandbox7760497960d8455ab8151afbd9a61c80.mailgun.org',
        'secret' => 'key-54eedcc7f733ebd95562d9855d6008e1',
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'github' => [
        'client_id' => '2e556cba4a309d02de4d',
        'client_secret' => '894ce852d0f93ed6105f2eeaa5ff487f2d478348', 
        'redirect' => env('GITHUB_REDIRECT','http://mooncake.cloud/auth/github/callback'),
    ],
    'google' => [
        'client_id' => '735153724442-ggp16021j386ea5m5gbdhv7d7vrc1umc.apps.googleusercontent.com',
        'client_secret' => 'cFpJtP1TOIuAm3DVuSBIVi9N', 
        'redirect' => env('GOOGLE_REDIRECT', 'http://mooncake.cloud/auth/google/callback'),
    ],

];
