<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the script version for js.
    */

    'script_version' => env('APP_SCRIPT_VERSION', '11'),

];