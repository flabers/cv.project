<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    public function tenants()
    {
        return $this->select('id', 'tenant_name')->distinct()->get();
    }
}
