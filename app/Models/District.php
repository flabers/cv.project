<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    function getDistricts()
    {
        return $this->select('district')->get();
    }
}
