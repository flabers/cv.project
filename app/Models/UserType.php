<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{

    protected $fillable = [
        'id', 'position'
    ];

    public function customers()
    {
        //return $this->hasMany(User::class);
        return $this->hasMany(User::class);
    }
    public function managers()
    {
        return $this->hasMany(User::class, 'user_type', 'id');
    }
    public function userTypes()
    {
       return $this->select(['id', 'position'])->get();
    }
}
