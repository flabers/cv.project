<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'github_id', 'github_avatar', 'github_id', 'google_avatar', 'user_type', 'user_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function customers()
    {
        return $this->with('user_types')->where('user_type', 9)->get();
    }
    public function position()
    {
        return $this->belongsTo(UserType::class, 'id');
    }
    public function checkUser($user_id)
    {
        return $this->where('github_id', $user_id)->first();
    }
    public function types()
    {
        return $this->hasOne(UserType::class, 'id', 'user_type');
    }
    public function managers()
    {
        return $this->select(['name', 'email', 'users.id', 'position', 'user_status'])->where('user_type', '<>', 9)->where('user_type', '<>', 1)->leftJoin('user_types', 'users.user_type', '.user_types.id')->get(); 
    }
    public function getManagersExecly()
    {
        return $this->select(['id', 'name'])->where('user_type', 2)->get();
    }
    public function manager($id)
    {
        return $this->select(['name', 'email', 'users.id', 'position', 'user_status'])->leftJoin('user_types', 'users.user_type', '.user_types.id')->where('users.id', $id)->first();
    }
    public function users()
    {
        return $this->select('id', 'name', 'user_type')->orderBy('id', 'ASC')->get();
    }
    public function getManagers()
    {
        return $this->select(['id', 'name'])->where('user_type', 2)->get();
    }
}
