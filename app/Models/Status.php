<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function statuses()
    {
        return $this->select('id', 'status_name')->orderBy('id', 'DESC')->get();
    }
}
