<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyLog extends Model
{
    public function getPropertyLogs($id)
    {
        return $this->select(
            [
                'property_logs.created_at',
                'property_logs.action_id',
                'users.name',
                'user_types.position'
            ])
            ->leftJoin('users', 'users.id', '=', 'property_logs.user_id')
            ->leftJoin('user_types', 'user_types.id', '=', 'users.user_type')
            ->where('realty_id', $id)
            ->orderBy('created_at', 'DESC')
            ->get();
    }
}
