<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Realty extends Model
{
    private $rows = [
        "status_id",
        "address",
        "district",
        "squere",
        "name",
        "type_id",
        "tenant_id",
        "type_name",
        "tenant_name",
        "end_date",
        "realties.id",
        "user_id",
        "realty_name",
        "start_date",
        "status_name",
        "tenants.phone",
        'number_object',
        'object_floor',
        'building_floors'

    ];
    public function tasks()
    {
        return $this->hasMany('App\Models\Engineer_task');
    }
    public function pictures()
    {
        return $this->hasMany('App\Models\Picture')->orderBy('sort');
    }
    public function logs()
    {
        return $this->hasMany('App\Models\PropertyLog');
    }
    public function objects()
    {
        return $this->all();
    }
    public function filterBySquare($query)
    {
        $realtis = $this->query();
        if (!$query->has('from') && !$query->has('to')) {
            return false;
        }
        $realtis = $realtis->select($this->rows)
            ->leftJoin('users', 'users.id', '=', 'realties.user_id')
            ->leftJoin('statuses', 'statuses.id', '=', 'realties.status_id')
            ->leftJoin('property_types', 'property_types.id', '=', 'realties.type_id')
            ->leftJoin('tenants', 'realties.tenant_id', '=', 'tenants.id');
        if (!is_null($query->input('from'))) {
            $realtis->where('squere', '>', $query->input('from'));
        }
        if (!is_null($query->input('to'))) {
            $realtis->where('squere', '<', $query->input('to'));
        }

        $realtis = $realtis->get();
        return $realtis;
    }
    public function conclusion()
    {
        return $this->leftJoin('users', 'users.id', '=', 'realties.user_id')
            ->leftJoin('property_types', 'property_types.id', '=', 'realties.type_id')
            ->leftJoin('tenants', 'tenants.id', '=', 'realties.tenant_id');
    }
    public function sortByStatus($status_id)
    {
        return $this->
            leftJoin('users', 'users.id', '=', 'realties.user_id')
                ->leftJoin('property_types', 'property_types.id', '=', 'realties.type_id')
                ->leftJoin('tenants', 'tenants.realty_id', '=', 'realties.id')
                ->where('status_id', $status_id)
                ->get();
    }


    public function getUserDetails($id)
    {
        return $this->select($this->rows)
            ->leftJoin('users', 'users.id', '=', 'realties.user_id')
            ->leftJoin('property_types', 'property_types.id', '=', 'realties.type_id')
            ->leftJoin('tenants', 'tenants.id', '=', 'realties.tenant_id')
            ->leftJoin('statuses', 'statuses.id', '=', 'realties.status_id')
            ->where('user_id', $id)
            ->get();
    }

    public function filterByMouth($query)
    {
        $realties = $this->query();
        $realties->select($this->rows)
            ->leftJoin('users', 'users.id', '=', 'realties.user_id')
            ->leftJoin('statuses', 'statuses.id', '=', 'realties.status_id')
            ->leftJoin('property_types', 'property_types.id', '=', 'realties.type_id')
            ->leftJoin('tenants', 'realties.tenant_id', '=', 'tenants.id');
        if (!is_null($query->input('month'))) {
            $realties->whereMonth('realties.end_date', $query->input('month'));
        } else {
            return false;
        }

        return $realties->get();
    }
    public function removeObjects($id)
    {
        try {
            DB::table('realties')->where('id', $id)->delete();
            DB::table('engineer_tasks')->where('realty_id', $id)->delete();
            $pictures = DB::table('pictures')->select('picture_name')->where('realty_id', $id)->get();
            if ($pictures->count()) {
                foreach ($pictures as $picture) {
                    unlink('uploads/' . $picture->picture_name);
                }
            }
            return true;
        } catch (Exeption $e) {
            return $e;
        }
    }
    public function getObjects($user)
    {
        return $this->select($this->rows)
                ->leftJoin('users', 'users.id', '=', 'realties.user_id')
                ->leftJoin('property_types', 'property_types.id', '=', 'realties.type_id')
                ->leftJoin('tenants', 'tenants.id', '=', 'realties.tenant_id')
                ->leftJoin('statuses', 'statuses.id', '=', 'realties.status_id')
                ->where('user_id', $user->id)
                ->with('tasks')
                ->get();
    }
    public function realtiesWithOutManager()
    {
        return $this->select($this->rows)
            ->leftJoin('users', 'users.id', '=', 'realties.user_id')
            ->leftJoin('property_types', 'property_types.id', '=', 'realties.type_id')
            ->leftJoin('tenants', 'tenants.id', '=', 'realties.tenant_id')
            ->leftJoin('statuses', 'statuses.id', '=', 'realties.status_id')
            ->whereNull('user_id')
            ->get();
    }
    public function getTotalAmount()
    {
        return $this->select('id')->get()->count();
    }
    public function liveSearch($search_string)
    {
        return $this->select($this->rows)
            ->leftJoin('users', 'users.id', '=', 'realties.user_id')
            ->leftJoin('statuses', 'statuses.id', '=', 'realties.status_id')
            ->leftJoin('property_types', 'property_types.id', '=', 'realties.type_id')
            ->leftJoin('tenants', 'realties.tenant_id', '=', 'tenants.id')
            ->where('address', 'like', '%' . $search_string . '%')
            ->orWhere('tenants.tenant_name', 'like', '%' . $search_string . '%')
            ->orWhere('tenants.phone', 'like', '%' . $search_string . '%')
            ->get();
    }
    public function rented()
    {
        return $this->where('status_id', 1)->get()->count();
    }

    public function unreliably()
    {
        return $this->where('status_id', 2)->get()->count();
    }
    public function unRented()
    {
        return $this->where('status_id', 3)->get()->count();
    }
    public function getCardRealty($id)
    {
        array_push($this->rows, 'phone', 'tenants.email');
        return $this->select($this->rows)
            ->leftJoin('users', 'users.id', '=', 'realties.user_id')
            ->leftJoin('property_types', 'property_types.id', '=', 'realties.type_id')
            ->leftJoin('statuses', 'statuses.id', '=', 'realties.status_id')
            ->leftJoin('tenants', 'realties.tenant_id', '=', 'tenants.id')
            ->with('pictures')
            ->with('logs')
            ->find($id);
    }

    public function filter($filter_condition)
    {
        $managers = [];
        $square = [];
        $districts = [];
        $statuses = [];

        foreach ($filter_condition as $key => $condition) {
            $temp = explode("_", $key);
            if (count($temp)) {
                if ($temp[0] == "manager") {
                    $managers[] = $temp[1];
                } elseif ($temp[0] == "district") {
                    $districts[] = $temp[1];
                } elseif ($temp[0] == "status") {
                    $statuses[] = $temp[1];
                }
            }
        }
        $realtis = $this->query();
        $realtis->select($this->rows)
            ->leftJoin('users', 'users.id', '=', 'realties.user_id')
            ->leftJoin('statuses', 'statuses.id', '=', 'realties.status_id')
            ->leftJoin('property_types', 'property_types.id', '=', 'realties.type_id')
            ->leftJoin('tenants', 'realties.tenant_id', '=', 'tenants.id');
        if (count($managers)) {
            $realtis->whereIn('user_id', $managers);
        }
        if (count($districts)) {
            $realtis->whereIn('district', $districts);
        }
        if (count($statuses)) {
            $realtis->whereIn('status_id', $statuses);
        }
        if (isset($filter_condition["from"])) {
            $realtis->where('squere', '>', $filter_condition["from"]);
        }
        if (isset($filter_condition["to"])) {
            $realtis->where('squere', '<', $filter_condition["to"]);
        }
        if (isset($filter_condition["month"])) {
            $realtis->whereMonth('tenants.end_rend', $filter_condition->month);
        }
        return $realtis->get();
    }
}
