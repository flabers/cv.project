<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyType extends Model
{
    
    public function getPropertyType()
    {
        return $this->select('id', 'type_name')->get();
    }
}
