<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function getComments($realty_id)
    {
        return $this->select([
            'name',
            'position',
            'comment',
            'comments.created_at'

        ])
            ->leftJoin('users', 'users.id', '=', 'comments.user_id')
            ->leftJoin('user_types', 'user_types.id', '=', 'users.user_type')
            ->where('realty_id', $realty_id)
            ->orderBy('comments.created_at', 'DESC')
            ->get();
    }
}
