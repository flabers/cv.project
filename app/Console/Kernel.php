<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Mail;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
       // \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $ended = [];
            $realtis = DB::table('realties')->select('end_data', 'id', 'realty_name')->where('status_id', '<>', 3)->get();
            foreach ($realtis as $realty) {
                $seconds = strtotime($realty->end_data) - time();
                $days = round(($seconds / 60) / 60 / 24);
                if($days < 60) {
                    $ended[] = [
                      'id' =>   $realty->id,
                      'realty_name'  => $realty->realty_name
                    ];
                    DB::table('realties')->where('id', $realty->id)->update([
                        'status_id' => 3
                    ]);
                }
            }
            if(count($ended) > 0) {
                Mail::send('emails.rent_end', array('end' => $ended), function($message)
                {
                    $message->to('toostupidmonkey@gmail.com', 'Джон Смит')->subject('Аренда заканчивается');
                });
            }

        })->weekly()->mondays()->at('13:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
