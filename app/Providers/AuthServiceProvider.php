<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Comment;
use App\Policies\CommentPolicy;
use App\Models\Picture;
use App\Policies\PicturePolicy;
use App\Policies\EngineertasksPolicy;
use App\Policies\UserPolicy;
use App\Models\Engineer_task;
use App\Models\User;
use Laravel\Passport\Passport;
use App\Policies\ModelPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Realty' => 'App\Policies\ObjectPolicy',
        Comment::class => CommentPolicy::class,
        Picture::class => PicturePolicy::class,
        User::class => UserPolicy::class,
        //User::class => ModelPolicy::class,
        Engineer_task::class => EngineertasksPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        Passport::tokensExpireIn(now()->addDays(15));

        Passport::refreshTokensExpireIn(now()->addDays(30));
    }
}
