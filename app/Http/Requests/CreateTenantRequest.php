<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTenantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required',
            'address' => 'required',
            'address' => 'required',
            'square' => 'required',
            'type' => 'required',
            'file.*' => 'image|mimes:jpeg,png,jpg,gif'
        ];
    }
}
