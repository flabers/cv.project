<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateManager extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.name' => 'string|max:255|required',
            'data.email' => 'E-Mail|unique:users,email|required',
            'data.password' => 'string|max:255|required',
            'data.user_type_id' => 'Numeric|required'
        ];
    }

}
