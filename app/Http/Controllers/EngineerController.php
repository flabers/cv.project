<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Engineer_task;
use Auth;
use App\Http\Requests\CreateTaskRequest;

class EngineerController extends Controller
{

    public function createTask(CreateTaskRequest $request)
    {
        $user = Auth::user();
        $task = new Engineer_task();
        if ($user->can('tasks', $task)) {
            $data_task = $task = new Engineer_task();
            $task->task = $request->new_task;
            $task->complete = $request->complete;
            $task->realty_id = $request->realty_id;
            $task->status = 'New';
            $task->start = date('Y-m-d', time());
            $task->user_id = Auth::user()->id;
            $save = $task->save();
            if (!$save) {
                return response()->json([
                    'message' => 'Task didn`t save'
                ], 501);
            }
            $data_task->name = Auth::user()->name;
            $data_task->user_type = Auth::user()->user_type;
            return response()->json([
                'task' => $data_task
            ], 200);
        }else {
            return response()->json([
                'message' => 'You donn`t have enough rules'
            ], 403);
        }
    }
    public function index($id)
    {
        $tasks = new Engineer_task();
        $tasks = $tasks->select([
                'users.name',
                'complete',
                'status',
                'start',
                'task',
                'engineer_tasks.id',
                'complete_engineer',
                'position'

            ])
                ->leftJoin('users', 'users.id', '=', 'engineer_tasks.engineer_id')
                ->leftJoin('user_types', 'user_types.id', '=', 'users.user_type')
                ->where('realty_id', $id)->get();
        return $tasks;
    }
    public function processTask(Request $request)
    {
        if (Auth::user()->user_type != 4) {
            return response()->json(
                [
                    'massage' => 'You haven`t enough rules to do this'
                ], 403
            );
        }
        $task = new Engineer_task();
        $task = $task->find($request->id);
        $task->status = 'In process';
        if($request->complete_engineer) {
            $task->complete_engineer = $request->complete_engineer;
        } else {
            return response()->json(
                [
                    'massage' => 'Data didn`t save'
                ], 500
            );
        }
        $task->engineer_id = Auth::user()->id;
        $save = $task->save();
        $task->name = Auth::user()->name;
        if ($save) {
            return response()->json(
                [
                    'massage' => 'task in process',
                    'data' => $task
                ], 200
            );
        } else {
            return response()->json(
                [
                    'massage' => 'Data didn`t save'
                ], 500
            );
        }
    }
    public function closeTask(Request $request)
    {
        $user = Auth::user();
        $task = new Engineer_task();
        if ($user->can('closeTask', $task)) {
            $task = new Engineer_task();
            $task = $task->find($request->id);
            $task->status = 'Closed';
            $task->engineer_id = Auth::user()->id;
            $save = $task->save();
            if ($save) {
                return response()->json(
                    [
                        'massage' => 'Task closed'
                    ], 200
                );
            } else {
                return response()->json(
                    [
                        'massage' => 'Data didn`t save'
                    ], 500
                );
            }
        } else {
            return response()->json(
                [
                    'massage' => 'You haven`t enough rules to do this'
                ], 403
            );
        }

    }
    public function completed(Request $request)
    {
        $id = $request->id;
        $task = new Engineer_task();
        $task = $task->find($id);
        $task->status = 'Completed';
        $save = $task->save();
        if (!$save) {
            return response()->json('Error while saving task status', 412);
        }
        return response()->json('Status updated', 200);
    }
}
