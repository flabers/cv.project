<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Realty;
use DB;
use PDF;
class PdfController extends Controller
{

    public function export($id)
    {
        if (is_null($id)) {
            return view('card', [
                'message' => 'Necessary parameters not exist'
            ], 412);
        }
        $realty = new Realty();
        $realty = $realty->getCardRealty($id);

        $realty_pictures = $realty->pictures;
        $temp = [];
        $result = [];
        if ($realty_pictures->count() > 0) {
            foreach ($realty_pictures as $key => $picture){
                $temp[] = $picture;
                if(count($temp) == 2) {
                    $result[] = $temp;
                    $temp = [];
                }
            }


        }
        if ($realty->count()) {
            $pdf = PDF::loadView('layouts.pdf', [
                'realty_pictures' => $result,
                'realty' => $realty,
                'id' => $id
            ]);
            return $pdf->download('object_'.$id.'.pdf');
        }
        return view('card', [
            'message' => 'Realty not found'
        ], 404);

    }
}
