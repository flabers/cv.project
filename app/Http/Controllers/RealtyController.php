<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Realty;
use App\Models\User;
use App\Models\Status;
use App\Models\PropertyLog;
use App\Models\Comment;
use App\Models\Tenant;
use App\Models\PropertyType;
use App\Models\District;
use DB;
use Validator;
use Auth;
use App\Traits\SupportFunctions;
use App\Http\Requests\RealtyFilterRequest;


class RealtyController extends Controller
{
    use SupportFunctions;



    public function index()
    {
        $users = new User();
        $district = new District();
        //get  users for table on home page
        $users = $users->users();

        $realties_by_users = [];
        foreach ($users as $user) {
            $realty = new Realty();
            $realties_temp = $realty->getObjects($user);
            if ($realties_temp->count()) {
                $temp = [];
                $temp['name'] = $user->name;
                $temp['data'] = [];
                foreach ($realties_temp as $property) {
                    if (count($property->tasks) > 0) {
                        foreach ($property->tasks as $task) {
                            if ($task->status == 'Новая') {
                                $property->new_task = true;
                            }
                        }
                    }
                    $temp['data'][] = $property;
                }
                $realties_by_users[] = $temp;
            }
        }
        $realtis = new Realty();

        $districts = $district->getDistricts();
        $total = $realtis->getTotalAmount();
        
        $rented = $realtis->rented();
        $unreliably = $realtis->unreliably();
        $not_rented = $realtis->unRented();

        $statuses = new Status();
        $statuses = $statuses->statuses();
        //realties with out manager
        $single_realties = $realtis->realtiesWithOutManager();
        return view('content', [
            'realties' => $realties_by_users,
            'not_reliable' => $unreliably,
            'rented' => $rented,
            'not_rented' => $not_rented,
            'total' => $total,
            'users' => $users,
            'statuses' => $statuses,
            'district' => $districts,
            'single_realties' => $single_realties
        ]);
    }

    public function removeObject($id)
    {
        $user = Auth::user();
        $object = Realty::find($id);
        if ($user->can('makeAction', $object)) {
            $result = $object->removeObjects($object->id);
            if ($result) {
                return response()->json(
                    [
                        'massage' => 'Object deleted'
                    ], 200
                );
            }
            return response()->json(
                [
                    'massage' => $result
                ], 403
        );
        } 
        
    }

    public function createTenant(Request $request)
    {
        try {
            $tenant = new Tenant();
            $tenant->tenant_name = $request->input('tenant_name');
            $tenant->phone = $request->input('phone');
            $tenant->email = $request->input('email');
            $save = $tenant->save();
            if (!$save) {
                return response()->json([
                    'msg' => 'Problem with save new tenant'
                ], 412);
            }
            DB::table('realties')
                ->where('id', $request->input('realty_id'))
                ->update(
                    [
                        'tenant_id' => $tenant->id,
                        'end_date' => $request->input('end_date'),
                        'start_date' => date('Y-m-d', time())
                    ]
                );
            //save log
            $log = new PropertyLog();
            $log->user_id = Auth::user()->id;
            $log->action_id = ' Создан новый арендатор ' . $tenant->tenant_name;
            $log->realty_id = $request->input('realty_id');
            $log->save();
            //send log
            $log = [
                'user_name' => Auth::user()->name,
                'user_role' => DB::table('user_types')
                    ->select('position')
                    ->join('users', 'users.user_type', '=', 'user_types.id')
                    ->where('users.id', Auth::user()->id)->first()->position,
                'action_id' => ' Создан новый арендатор ' . $tenant->tenant_name,
                'created_at' => $tenant->created_at
            ];
            return app('App\Http\Controllers\Api\RealtyController')->getCardRealty($request->input('realty_id'));
        } catch (Exception $e) {
            return response()->json([
                'msg' => 'Problem with save new tenant',
                'error' => $e
            ], 412);
        }


    }

    public function getCardRealty($id)
    {

        if (is_null($id)) {
            return view('card', [
                'message' => 'Necessary parameters not exist'
            ], 412);
        }
        $realty = new Realty();

    
        $realty = $realty->getCardRealty($id);


        $this->writeOpenLog($id, $realty->realty_name);

  
        $comments = $this->getComments($id);
        $days = $this->getDaysLeft($realty->end_date);

        $districts = new District();
        $district = $districts->getDistricts();

        $statuses = new Status();
        $statuses = $statuses->statuses();

        $property_type = new PropertyType();
        $types = $property_type->getPropertyType();

        $tenant = new Tenant();
        $tenants = $tenant->tenants();
        $users = new User();
        $managers = $users->getManagers();
        if ($realty->count()) {
            return view('card', [
                'realty' => $realty,
                'comments' => $comments,
                'days_left' => $days,
                'districts' => $district,
                'statuses' => $statuses,
                'types' => $types,
                'tenants' => $tenants,
                'tasks' => app('App\Http\Controllers\EngineerController')->index($id),
                'managers' => $managers
            ]);
        }
        return view('card', [
            'message' => 'Realty not found'
        ], 404);
    }

    public function filter(Request $request)
    {
        $realtis = new Realty();
        $condition = $request->all();
        $realtis = $realtis->filter($condition);
        return response()->json([
            'realties' => $realtis,
        ], 200);
    }

    public function temp()
    {
        $realtis = new Realty();
        $users = new User();
        $users = $users->select('id', 'name')->orderBy('id', 'ASC')->get();
        $statuses = new Status();
        $statuses = $statuses->select('status_name', 'id')->get();
        $districts = $realtis->select('district')->groupBy('district')->get();

        return view('welcome', [
            'statuses' => $statuses,
            'users' => $users,
            'districts' => $districts
        ]);
    }

    public function filterBySquare(RealtyFilterRequest $request)
    {
        $realties = new Realty();
        $realties = $realties->filterBySquare($request);
        if (!$realties) {
            return  response()->json([
                'message' => 'Necessary parameters not exist'
            ], 412);
        }
        return response()->json([
            'realties' => $realties,
        ], 200);
    }

    public function getRealtyByMonth(Request $request)
    {
        $realties = new Realty();
        $realties = $realties->filterByMouth($request);

        if (!$realties) {
            return response()->json([
                'message' => 'Necessary parameters not exist'
            ], 412);
        }
        return response()->json([
            'realties' => $realties,
        ], 200);
    }

    public function sortRealtyByStatus()
    {
        $realties = new Realty();
        $status = new Status();
        $statuses = $status->statuses();
        $realties_by_status = [];

        foreach ($statuses as $status) {
            $realties_temp = $realtis->sortByStatus($status->id);
            if ($realties_temp->count()) {
                $temp = [];
                foreach ($realties_temp as $property) {
                    $temp[] = $property;
                }
                $realties_by_status[] = $temp;
            }
        }
        $conclusion = $realties->conclusion();
        $total = $realties->getTotalAmount();
        $rented = $conclusion->where('status_id', 1)->get()->count();
        $unreliably = $conclusion->where('status_id', 2)->get()->count();
        $not_rented = $conclusion->where('status_id', 3)->get()->count();
        return response()->json([
            'total_realties' => count($realtis),
            'rented' => $rented,
            'not_rented' => $not_rented,
            'total' => $total,
            'realties' => $realties_by_status,

        ], 200);
    }

    public function liveSearch(Request $request)
    {
        $search_string = $request->input('search');
        $realties = new Realty();
        $realties = $realties->liveSearch($search_string);
        return response()->json([
            'realties' => $realties
        ], 200);
    }

    public function insertComment(Request $request)
    {
        $user = Auth::user();
        if ($user->can('insertComment', Comment::class)) {
            $comment = $request->input('comment');
            $realty_id = $request->input('realty_id');
            $user_id = $request->input('user_id');
            $comments = new Comment();
            $comments->user_id = $user_id;
            $comments->comment = $comment;
            $comments->realty_id = $realty_id;
            $saved = $comments->save();
            $comments->name = Auth::user()->name;
            if (!$saved) {
                return response()->json([
                    'message' => 'Comment did`n save'
                ], 501);
            }
            $position = DB::table('user_types')->select('position')->find(Auth::user()->user_type);
            $comments->position = $position->position;
            return response()->json([
                'message' => 'Comment saved',
                'comment' => $comments
            ], 201);
        } else {
            return response()->json(
                [
                    'massage' => 'You haven`t enough rules to do this'
                ], 403
            );
        }
    }

    public function getComments($realty_id)
    {
        $comments = new Comment();
        $comments = $comments->getComments($realty_id);
        if ($comments->count()) {
            return $comments;
        }
        return false;
    }

    public function updateRealty(Request $request)
    {
        $user = Auth::user();
        $object = Realty::find($request->input('id'));
        if ($user->can('makeAction', $object)) {
            $model_name = strtolower($request->input('name'));
            $colume = $request->input('colume');
            $id = $request->input('id');
            $value = $request->input('value');
            $data = $request->input('data');
            $user_id =  Auth::user()->id;

            $responce = $this->writeLog($data, $user_id, $id);

            if ($request->tenant_id) {
                $id = $request->input('tenant_id');
            }

            $saved = DB::table($model_name)
                ->where('id', $id)
                ->update([$colume => $value]);

            if (!$saved) {
                return response()->json([
                    'message' => 'Data did`n save'
                ], 501);
            }
            //add manager name
            $responce['user_name'] = Auth::user()->name;

            try {
                $responce['user_role'] = DB::table('user_types')
                    ->select('position')
                    ->join('users', 'users.user_type', '=', 'user_types.id')
                    ->where('users.id', Auth::user()->id)->first()->position;
            } catch (Exception $e) {
                return response()->json([
                    'msg' => 'Position not found',
                    'error' => $e
                ], 412);
            }
            $value_id = '';
            try {
                if ($data['name'] == 'Тип') {
                    $value = DB::table('property_types')
                        ->select('type_name')
                        ->find($value)->type_name;
                } elseif ($data['name'] == 'user_id') {
                    $value = DB::table('users')
                        ->select('name')
                        ->find($value)->name;
                } elseif ($data['name'] == 'Статус') {
                    $value_id = $value;
                    $value = DB::table('statuses')
                        ->select('status_name')
                        ->find($value)->status_name;
                } else {
                    if ($data['name'] == 'Орендатор') {
                        if ($value) {
                            $value = DB::table('tenants')
                                ->select('tenant_name')
                                ->find($value)->tenant_name;
                        } else {
                            $value = 'Не выбран';
                        }

                    }
                }
            } catch (Exception $e) {
                return response()->json([
                    'msg' => 'Problem with save new tenant',
                    'error' => $e
                ], 412);
            }
            return app('App\Http\Controllers\Api\RealtyController')->getCardRealty($id);
        }
        return response()->json(
            [
                'massage' => 'You haven`t enough rules to do this'
            ], 403
        );
    }

    public function userDetails($id)
    {
        $realties = new Realty();
        $realties = $realties->getUserDetails($id);
        return view('user', [
            'user_name' => $realties[0]->name,
            'realties' => $realties
        ]);
    }

}
