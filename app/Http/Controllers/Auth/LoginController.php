<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\Models\User;
use Auth;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('github')->user();
        } catch (Exception $e) {
            return Redirect::to('auth/github');
        }

        $authUser = $this->findOrCreateUserGithub($user);

        Auth::login($authUser, true);

        return Redirect::to('/user');
    }

    public function handleProviderCallbackGoogle()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (Exception $e) {
            return Redirect::to('auth/google');
        }

        $authUser = $this->findOrCreateUserGoogle($user);

        Auth::login($authUser, true);

        return Redirect::to('/user');
    }
    
    private function findOrCreateUserGithub($User)
    {
        if ($authUser = User::Where('github_id', $User->id)->first()) {
            return $authUser;
        } 
        else if ($authUser = User::Where('email', $User->email)->first()){
            if($authUser->github_id != $User->id) {
                $authUser->github_id = $User->id;
                $authUser->github_avatar = $User->avatar;
                $authUser->save();   
            }
            return $authUser;
        }
        return User::create([
            'name' => $User->nickname,
            'email' => $User->email,
            'password' => encrypt(str_random(8)),
            'github_id' => $User->id,
            'github_avatar' => $User->avatar,
            'user_status' => 1,
            'user_type' => 3
        ]);
    }

    private function findOrCreateUserGoogle($User)
    {
        if ($authUser = User::Where('google_id', $User->id)->first()) {
            return $authUser;
        } 
        else if ($authUser = User::Where('email', $User->email)->first()){
            if($authUser->google_id != $User->id) {
                $authUser->google_id = $User->id;
                $authUser->google_avatar = $User->avatar;
                $authUser->save();   
            }
            return $authUser;
        }
        return User::create([
            'name' => $User->name,
            'email' => $User->email,
            'google_id' => $User->id,
            'password' => encrypt(str_random(8)),
            'google_avatar' => $User->avatar,
            'user_status' => 1,
            'user_type' => 3
        ]);
    }

}
