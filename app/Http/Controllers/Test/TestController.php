<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Requset;

class TestController extends Controller
{
    public function store( $request)
    {
        $user = Auth::user();
        $realty = Realty::find($request->input('realty_id'));
        $id = $request->input('realty_id');
        $files = $request->file('file');
        if (!is_null($request->file)) {
            foreach ($files as $file) {
                if (file_exists('uploads/' . $file->getClientOriginalName())) {
                    $name = str_random(5) . '_' . $file->getClientOriginalName();
                } else {
                    $name = $file->getClientOriginalName();
                }
                $path = 'uploads/' . $name;
                Image::make($file->getRealPath())->resize(null, 600, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path);

                $picture = new Picture();
                $picture->picture_name = $name;
                $picture->realty_id = $id;
                $picture->save();
            }
        }

        return redirect('user/card/' . $id);
    } 
}
