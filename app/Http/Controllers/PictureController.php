<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use App\Models\Picture;
use Auth;
use DB;
use Image;
use App\Models\Realty;
use App\Http\Requests\PictureRequest;

class PictureController extends Controller
{


    public function index($id)
    {
        return view('upload',
            [
                'realty_id' => $id
            ]
        );
    }

    public function store(PictureRequest $request)
    {
        $user = Auth::user();
        $realty = Realty::find($request->input('realty_id'));
        if ($user->can('makeAction', $realty)) {
        $id = $request->input('realty_id');
        $files = $request->file('file');
        if (!is_dir('uploads')) {
            mkdir('uploads');
        }
        if (!is_null($request->file)) {
            foreach ($files as $file) {
                if (file_exists('uploads/' . $file->getClientOriginalName())) {
                    $name = str_random(5) . '_' . $file->getClientOriginalName();
                } else {
                    $name = $file->getClientOriginalName();
                }
                $path = 'uploads/' . $name;
                Image::make($file->getRealPath())->resize(null, 600, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path);

                $picture = new Picture();
                $picture->picture_name = $name;
                $picture->realty_id = $id;
                $picture->save();
            }
        }

        return redirect('user/card/' . $id);
    } 
    return response()->json([
        'msg' => 'You don`t have permisition'
    ], 400);
    }
    public function removePicture(Request $request)
    {

        if($request->id) {
            $user = Auth::user();
            $realty = Realty::find(Picture::find($request->id)->realty_id);
            if ($user->can('makeAction', $realty)) {
                $id = $request->id;
                $picture = new Picture();
                $file_name = DB::table('pictures')->select('picture_name')->where('id', $id)->first();
                if ($file_name) {
                    $file_name = $file_name->picture_name;
                    $delete =  unlink('uploads/'.$file_name);
                    $picture = $picture->find($id)->delete();
                    if ($picture) {
                        return response()->json('Картинка Удалена', 200);
                    }
                }
            }
            return response()->json('Картинка не  Удалена', 500);
        }
        return response()->json('Ощыбка при удаленни', 412);

    }
    public function updateSortPicture(Request $request, $id)
    {
        $user = Auth::user();
        $realty = Realty::find($id);
        if ($user->can('makeAction', $realty)) {
            $pictures = $request->data;
            foreach ($pictures["pictures"] as $picture) {
                DB::table('pictures')->where('id', $picture['id'])->update(
                    [
                        'sort' => $picture['sort']
                    ]
                );
            }
            return response()->json(
                [
                    'msg' => 'Сортировка обновлена'
                ], 200
            );
        }
        return response()->json(
            [
                'msg' => 'У вас не достаточно прав'
            ], 403
        );
    }


}
