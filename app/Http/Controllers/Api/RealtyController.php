<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\Realty;
use App\Models\Status;
use App\Models\PropertyType;
use App\Models\Tenant;
use App\Models\User;
use App\Traits\SupportFunctions;
use Auth;


class RealtyController extends Controller
{
    use SupportFunctions;

    public function getCardRealty($id)
    {
        if (is_null($id)) {
            return view('card', [
                'message' => 'Necessary parameters not exist'
            ], 412);
        }
        $realty = new Realty();

        $days = $this->getDaysLeft($realty->end_date);
    
        $realty = $realty->getCardRealty($id);

        $districts = new District();
        $district = $districts->getDistricts();

        $statuses = new Status();
        $statuses = $statuses->statuses();

        $property_type = new PropertyType();
        $types = $property_type->getPropertyType();

        $tenant = new Tenant();
        $tenants = $tenant->tenants();

        $users = new User();
        $managers = $users->getManagers();
        $user = Auth::user();
        $realty['edit_permission'] = $user->can('makeAction', $realty);
        if ($realty->count()) {
            return response()->json([
                'data' => [
                    'realty' => $realty,
                    'days_left' => $days,
                    'districts' => $district,
                    'statuses' => $statuses,
                    'types' => $types,
                    'tenants' => $tenants,
                    'managers' => $managers
                ]
            ]);
        }
        return response()->json([
            'message' => 'Realty not found'
        ], 404);
    }
}
