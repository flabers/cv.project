<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Realty;


class ObjectController extends Controller
{
    public function objects()
    {
        $objects = new Realty();
        return response()->json(['data' => $objects->objects()], 200);
    }
}
