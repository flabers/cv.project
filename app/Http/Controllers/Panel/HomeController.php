<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Realty;

class HomeController extends Controller
{
    public function conclusion()
    {
        $managers = new User();
        $managers = $managers->managers();
        $objects = new Realty();
        $objects = $objects->objects();
        return response()->json([
            'data' => [
                'managers'=> $managers,
                'objects' => $objects
            ]
        ], 200);

    }
}
