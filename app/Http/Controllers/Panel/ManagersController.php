<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserType;
use App\Http\Requests\ManagerUpdateRequest;
use App\Http\Requests\CreateManager;
use App\Http\Requests\ManagerStatusRequest;

class ManagersController extends Controller
{
    public function show()
    {
        $managers = new User();
        $managers = $managers->managers();
        if (count($managers) > 0) {
            return response()->json([
                'data' => $managers
            ], 200);
        }
        return response()->json([
            'message' => 'Managers not found'
        ], 404);
    }
    public function manager($id)
    {
        $manager = new User();
        $manager = $manager->manager($id);
        if (count($manager) > 0) {
            return response()->json([
                'data' => $manager
            ], 200);
        }
        return response()->json([
            'message' => 'Manager not found'
        ], 404);
    }
    public function update($id, ManagerUpdateRequest $request)
    {
        $colume_name = $request->data['colume_name'];
        $manager = new User();
        $manager = $manager->find($id);
        $manager->$colume_name = $request->ecrypt ? encrypt($request->data[$colume_name]) : $request->data[$colume_name];
        $save = $manager->save();
        if ($save) {
            return response()->json([
                'data' => $manager->manager($manager->id)
            ], 200);
        }
        return response()->json([
                'msg' => 'Something goes wrong'
            ], 403);

    }
    public function userTypes()
    {
        $types = new UserType();
        $types = $types->userTypes();
        if ($types->count() > 0) {
            return response()->json([
                'data' => $types
            ], 200);
        }
        return response()->json([
            'data' => []
        ], 200);

    }
    public function create(CreateManager $request) 
    {
        $user = new User();
        $user->name = $request->data['name'];
        $user->email = $request->data['email'];
        $user->user_type = $request->data['user_type_id'];
        $user->password = encrypt($request->data['password']);
        $save = $user->save();
        if ($save) {
            return response()->json([
                'data' => $user->manager($user->id)
            ], 200);
        }
        return response()->json([
                'msg' => 'Error bro'
            ], 503);
    }
    public function remove($id)
    {
        $manager = User::find($id);
        $manager->delete();
        $managers = $manager->managers();
        if (count($managers) > 0) {
            return response()->json([
                'data' => $managers
            ], 200);
        }
        return response()->json([
            'message' => 'Managers not found'
        ], 404);
    }
    public function setStatus(ManagerStatusRequest $request)
    {
        $manager =  new User();
        $manager = $manager->find($request->id);
        $manager->user_status = $request->user_status;
        $save = $manager->save();
        if ($save) {
            return response()->json([
                'data' => $manager->manager($manager->id)
            ], 200);
        }
        return response()->json([
                'data' => $save
        ], 503);
    }
}
