<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserType;

class CustomersController extends Controller
{
    public function index()
    {
        $customers = UserType::find(9)->customers;
        if (count($customers) > 0) {
            return response()->json([
                'data' => $customers
            ], 200);
        }
        return response()->json([
            'message' => 'Customers not found'
        ], 404);
        
    }
}
