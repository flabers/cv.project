<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'text' => 'required'
        ]);

        return Task::create([
            'text' => $request->text,
            'user_id' => auth()->user()->id,
            'is_completed' => false
        ]);
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return response()->json(['message' => 'Task deleted'], 200);
    }

    public function update(Task $task)
    {
        return tap($task)->update(request()->only(['is_completed', 'text']))->fresh();
    }
}

?>