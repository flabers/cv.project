<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Realty;
use DB;
use Auth;
use App\Models\Tenant;
use App\Models\User;
use App\Models\PropertyType;
use App\Models\District;
use App\Models\Status;
use Exception;
use Storage;
use App\Models\Picture;
use Image;
use App\Http\Requests\CreateTenantRequest;

class CreateController extends Controller
{

    public function index()
    {
        $district = new District();
        $districts = $district->getDistricts();

        $statuses = new Status();
        $statuses = $statuses->statuses();

        $property_types = new PropertyType();
        $property_types = $property_types->getPropertyType();

        $tenants = new Tenant();
        $tenants = $tenants->tenants();

        $users = new User();
        $managers = $users->getManagersExecly();

        return view('create', [
            'districts' => $districts,
            'statuses' => $statuses,
            'property_types' => $property_types,
            'tenants' => $tenants,
            'managers' => $managers
        ]);
    }

    public function save(CreateTenantRequest $request)
    {
        $tenant_id = $request->tenant;
        try {
            if ($request->input('tenant_checker') == "on") {
                $tenant = new Tenant();
                $tenant->tenant_name = $request->input('new_tenant');
                $tenant->phone = $request->input('phone');
                $tenant->email = $request->input('email');
                $tenant->save();
                $tenant_id = $tenant->id;
            }

            $realty = new Realty();
            $realty->status_id = $request->input('status');
            $realty->address = $request->input('address');
            $realty->district = $request->input('district');
            if (Auth::user()->user_type == 2) {
                $realty->user_id = Auth::user()->id;
            } elseif (Auth::user()->user_type == 3) {
                $realty->user_id = $request->manager;
            }

            $realty->squere = $request->input('square');
            $realty->type_id = $request->input('type');
            $realty->building_floors = $request->input('building_floors');
            $realty->number_object = $request->input('number_object');
            $realty->object_floor = $request->input('object_floor');
            $realty->end_date = $request->input('end_date');
            $realty->realty_name = $request->input('realty_name');
            $realty->start_date = date('Y-m-d', time());
            $realty->tenant_id = $tenant_id;
            $save = $realty->save();
            $id = $realty->id;

            $files = $request->file('file');


            if (!is_null($request->file)) {
                foreach ($files as $file) {
                    if (file_exists('uploads/' . $file->getClientOriginalName())) {
                        $name = str_random(5) . '_' . $file->getClientOriginalName();
                    } else {
                        $name = $file->getClientOriginalName();
                    }
                    $path = 'uploads/' . $name;
                    Image::make($file->getRealPath())->resize(null, 600, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($path);

                    $picture = new Picture();
                    $picture->picture_name = $name;
                    $picture->realty_id = $id;
                    $picture->save();
                }
            }

            if (!$save) {
                return response()->json([
                    'msg' => 'Save error'
                ], 401);
            }
            return redirect()->action(
                'RealtyController@getCardRealty', ['id' => $id]
            );
        } catch (Exception $e) {
            return back()->withInput();
        }
    }
}
