<?php

function formatDate($date) {
    return date('H:i d.m.Y', strtotime($date));
}
function formatDateOnly($date) {
    return date('d.m.Y', strtotime($date));
}