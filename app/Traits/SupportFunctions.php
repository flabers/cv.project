<?php

namespace App\Traits;
use App\Models\PropertyLog;
use Auth;
//use Mail;
trait SupportFunctions {

    public function writeLog($data, $user_id, $realty_id)
    {
        $PropertyLog = new PropertyLog();
        $PropertyLog->action_id = "Cмена ".$data['name']." &nbsp; c  &nbsp;". $data["old"]. " &nbsp; на &nbsp; ".$data["new"];
        $PropertyLog->user_id = $user_id;
        $PropertyLog->realty_id = $realty_id;
        $PropertyLog->created_at = date('Y-m-d H:i:s', time());
        $response = $PropertyLog;
        $PropertyLog->save();
        return $response;
    }
    public function writeOpenLog($realty_id, $realty_name)
    {
        $PropertyLog = new PropertyLog();
        $PropertyLog->action_id = "User ".Auth::user()->name . " looked through the page <a href='".$realty_id."'>".$realty_name."</a>";
        $PropertyLog->user_id = Auth::user()->id;
        $PropertyLog->realty_id = $realty_id;
        $PropertyLog->created_at = date('Y-m-d H:i:s', time());
        $response = $PropertyLog;
        $PropertyLog->save();
        return $response;
    }
    public function getDaysLeft($date)
    {
        $seconds = strtotime($date) - time();
        $days = round(($seconds / 60) / 60 / 24);
//        if($days < 60) {
//            Mail::send('emails.reminder', ['user' => $user], function ($m) use ($user) {
//                $m->from('hello@app.com', 'Your Application');
//                $m->to($user->email, $user->name)->subject('Your Reminder!');
//            });
//        }
        if ($days < 1) {
            $days = "Срок оренды истек ".abs($days)." дней назад";
        } else {
            $days = "сталось ".$days ." дней";
        }
        return $days;
    }
}