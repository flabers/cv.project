<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PicturePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function picturesSort(User $user, Realty $realty)
    {
        if ((in_array($user->user_type, [2, 3]) OR $realty->user_id == $user->id) AND $user->user_status) {
            return true;
        }
        return false;
    }
}
