<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Realty;

use Illuminate\Auth\Access\HandlesAuthorization;

class ObjectPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function makeAction(User $user, Realty $realty)
    {
        if ((in_array($user->user_type, ['2', '3']) OR $realty->user_id == $user->id) AND $user->user_status) {
            return true;
        } else {
            return false;
        }
    }
    public function addObject(User $user)
    {
        return in_array($user->user_type, ['2', '3']) AND $user->user_status;
    }
    public function observePermission(User $user, Realty $realty)
    {
        if ((in_array($user->user_type, ['1', '2', '3']) OR $realty->user_id == $user->id) AND $user->user_status) {
            return true;
        }

        return false;
    }
}
