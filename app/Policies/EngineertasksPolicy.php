<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EngineertasksPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function tasks(User $user)
    {
        return in_array($user->user_type, ['1', '2', '3']);
    }
    public function closeTask()
    {
        return in_array($user->user_type, ['1', '2', '3', '4']);
    } 
}
