<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */

     public function adminPanel(User $user)
     {
         return in_array($user->user_type, [1, 2, 3]);
     }

     public function permission(User $user)
     {
        return $user->user_type == 3 OR $user->user_type == 2;
     }
}
