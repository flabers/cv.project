let mix = require('laravel-mix');
let card_mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/app_upload.js', 'public/js')
    .sass('resources/assets/sass/app_card.scss', 'public/css')
    .sass('resources/assets/sass/media_card.scss', 'public/css')

mix.js('resources/assets/js/app_cv.js', 'public/js')   