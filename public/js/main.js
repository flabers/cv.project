var main = (function () {
    return { // методы доступные извне
        init: function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                }
            });

        },
        addOneMoreInput: function (counter, realty_id) {
            ++counter;
            str = '<div class="form-group">' +
                '<input type="hidden" value="' + realty_id + '" name="realty_id">' +
                '<label for="file"></label>' +
                '<input class="form-control-file" id="file" type="file" name="file_' + counter + '">' +
                '</div>';
            $(".input-counter").text(counter);
            $(".picture-inputs").append(str);

        },
        renderRealties: function (data) {
            $('.content-table').hide();
            $('.content-table').eq(0).show();
            var j = 1;

            for (var i = 0; i < data.length; i++) {
                if ($('.realty-content').length) {
                    var id = data[i].id;
                    var str = '<tr><td><a href="/user/card/' + id + '">' + j + '</a></td>' +
                        '<td><a href="/card/' + id + '">' + data[i].address + '</a>' +
                        '</td><td><a href="/user/card/' + id + '">' + data[i].squere + '</a>' +
                        '</td><td><a href="/user/card/' + id + '">' + data[i].type_name + '</a>' +
                        '</td><td><a href="/user/card/' + id + '">' + data[i].tenant_name + '</a>' +
                        '</td><td><a href="/user/card/' + id + '">' + data[i].start_date + '</a>' +
                        '</td><td><a href="/user/card/' + id + '">';
                    if (data[i].status_id == 1) {
                        str += '<span class="status green "></span>';
                    } else if (data[i].status_id == 2) {
                        str += '<span class="status yellow "></span>';
                    } else if (data[i].status_id == 3) {
                        str += '<span class="status red "></span>';
                    }

                    str += '</a></td></tr>';
                    $('.realty-content').eq(0).append(str);
                    ++j;
                } else {
                    var id = data[i].id;
                    var str = '<div class="search-wraper-popup"><a href="' + id + '" target="_blank">' + data[i].realty_name + '</a></div>';
                    $(".search-result-popup").append(str);
                }
            }
            $('.table-header').hide();
        },
        liveSearch: function (str) {
            $.ajax({
                'url': "user/search",
                'method': 'POST',
                'data': {
                    search: str
                },
                success: function (data) {
                    $('.realty-content').empty();
                    $(".search-result-popup").empty();
                    main.renderRealties(data.realties);
                    if (!data.realties.length && $('.realty-content').length) {
                        $(".search-result-popup").hide();
                    } else if (!$('.realty-content').length && data.realties.length) {
                        $(".search-result-popup").show();
                    }
                },
                error: function (e) {
                    console.log('Error: ' + e);
                }
            });
        },
        sendNotification: function (title, options) {
            if (!("Notification" in window)) {
                alert('Ваш браузер не поддерживает HTML Notifications, его необходимо обновить.');
            } else if (Notification.permission === "granted") {
                var notification = new Notification(title, options);

                function clickFunc() {
                    alert('Пользователь кликнул на уведомление');
                }

                notification.onclick = clickFunc;
            } else if (Notification.permission !== 'denied') {
                Notification.requestPermission(function (permission) {
                    if (permission === "granted") {
                        var notification = new Notification(title, options);

                    } else {
                        alert('Вы запретили показывать уведомления'); // Юзер отклонил наш запрос на показ уведомлений
                    }
                });
            } else {

            }
        }
    }
}());
$(document).ready(function () {
    main.init();
    $(".filter-button-mobile").on('click', function () {
        $(".filter-sidebar").toggleClass('d-none');
    });
    $(".add-one-more").on('click', function () {
        counter = $(".input-counter").text();
        realty_id = $(".realty_id").text();
        main.addOneMoreInput(counter, realty_id);
    });
    $("#live-search").on('keyup', function () {
        var str = $(this).val();
        main.liveSearch(str);
        $('.table-header').hide();
    });
    $(".collapse").on('click', function () {
        $('.collapse').removeClass('show');
        $(this).addClass('show');
    });
    $('.close-modal-window').on('click', function () {
        $(this).closest('.modal').hide();
    });

    $(document).mouseup(function (e) {
        var container = $(".search-result-popup");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.hide();
        }
    });
});