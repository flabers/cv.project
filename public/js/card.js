var card = (function () {

    return {
        //иницыальзацыя отправления токена при каждом аякс запросе
        init: function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                }
            });

        },
        addComment: function (obj) {
            var data = obj.serialize();
            try {
                $.ajax({
                    'url': 'comment',
                    'type': "POST",
                    'data': data,
                    dataType: "JSON",
                    success: function (data) {
                        var comment = '' +
                            '<div class="container"><div class="row history-line"><div class="col-xl-3">' +
                            '<p class="font-weight-bold">' + data.comment.name + '</p><div class="history-date"><span class="sub-info-log">' + data.comment.position + '</span>' +
                            '</div><div class="history-position"><span class="sub-info-log">' + data.comment.created_at + '</span>' +
                            '</div></div><div class="col-xl-9 col-offset-4"><p>' + data.comment.comment + '</p></div></div></div>';
                        $(".comments").prepend(comment);
                        $(".comment-form").find('textarea').val('');
                        $(".comments-header").show();
                    },
                    error: function (e) {
                        console.log("Error: " + e);
                    }
                });
            } catch (e) {
                console.error('Error: ' + e);
                return false;
            }
        },
        changeStatus: function (data) {
            try {
                $.ajax({
                    'url': 'update-realty',
                    'type': "POST",
                    'data': data,
                    dataType: "JSON",
                    success: function (data) {
                        $(".main-status-indicator").attr('class', 'main-status-indicator');
                        if (data.value_id == 3) {
                            $(".main-status-indicator").addClass("status red");
                        } else if (data.value_id == 2) {
                            $(".main-status-indicator").addClass("status yellow");
                        } else if (data.value_id == 1) {
                            $(".main-status-indicator").addClass("status green");
                        }
                        card.renderLogs(data.log);
                        $(".name-current-status").text(data.message);
                        $(".change-status").hide();
                    },
                    error: function (e) {
                        console.log("Error: " + e);
                    }
                });
            } catch (e) {
                console.error('Error: ' + e);
                return false;
            }
        },

        hideEditBlock: function (elem) {
            elem.hide();
        },
        deletePicture: function (id) {
            $.ajax({
                url: 'remove-picture',
                type: "POST",
                data: {
                    id: id
                },
                dataType: "JSON",
                success: function (data) {
                    alert(data);
                },
                error: function (e) {
                    alert(e);
                }
            });
        },
        removeObject: function (id) {
            $.get('remove-object/' + id, function (data) {
                window.location.href = '/';
            });
        }

    }
}());
$(document).ready(function () {
    card.init();
    $(".comment-form").on('submit', function () {
        var obj = $(this);
        card.addComment(obj);
        return false;
    });
    $('.hide-edit-block').on('click', function () {
        var elem = $(this).closest('.input-edit-block');
        card.hideEditBlock(elem);
    })

    $(".change-realty-status").on('click', function () {
        var obj = $(this);
        //$(".main-status-indicator").attr('current-status', obj.attr('status').trim());
        var data = {
            colume: 'status_id',
            id: obj.data('id'),
            value: obj.data('status_id'),
            name: 'realties',
            data: {
                name: 'Статус',
                old: $(".name-current-status").text(),
                new: obj.data('status_name'),
            },
            user_id: obj.data('user')
        };
        card.changeStatus(data);
    });


    //code for showing edit blocks
    $(".show-eidt-block").on('click', function () {
        $('.input-edit-block').hide();
        $(this).siblings('.input-edit-block').show();
        return false;
    });

    $(".change-status-button").on('click', function () {
        $('.change-status').slideToggle();
    });

    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        slidesPerView: 1,
        centeredSlides: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    $('.delete-picture').on('click', function () {
        var id = $(this).data('id');
        card.deletePicture(id);
        $(this).closest('a').remove();
        return false;
    });
    $(".manager-edit").on('click', function () {
        var obj = $(this);
        var data = {
            colume: 'user_id',
            id: obj.data('id'),
            value: $("#manager").val(),
            name: 'realties',
            data: {
                name: 'user_id',
                old: $(".old-manager-value").text().trim(),
                new: $("#manager").val(),
            },
            user_id: obj.data('user')
        };
        card.changeManager(data);
        //$(this).closest('manager-wrap').find('.select-word').hide();
    });
    $('.remove-object').on('click', function () {
        var id = $(this).data('id');
        card.removeObject(id);
    });
    $("#sortable").sortable();
    $('.save-sort').on('click', function () {
        var pictures = $(".picture-block");
        let realty_id = $(this).data('id')
        var data = {
            pictures: []
        };
        for (var i = 0; i < pictures.length; ++i) {
            data.pictures.push({
                id: pictures.eq(i).data('id'),
                sort: pictures.eq(i).index()
            });
        }
        $.post('sort/' + realty_id, {
            data: data
        }, 'json').done(function (msg) {
            alert(msg.msg)
            //location.reload();
        }).fail(function (xhr, status, error) {
            alert(error);
        });
    });
    $(".existing-tenants").select2();
});