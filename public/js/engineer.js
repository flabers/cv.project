var engineer = (function () {

    return { // методы доступные извне
        status_ajax: true,
        elem: '',
        //иницыальзацыя отправления токена при каждом аякс запросе
        init: function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        },
        bindHendlers: function () {
            $(".show-engineer-date").on('click', function () {
                var obj = $(this).closest('.lead').find('.complete_engineer');
                engineer.showEngineerInput(obj);
            });
            $('.close-task').on('click', function () {
                var id = $(this).data('id');
                engineer.closeTask(id);
                $(this).closest('.card').remove();
                return false;
            });
            $('.process').on('click', function () {
                var obj = $(this);
                var engineer_date = obj.closest('.card').find('.complete_engineer');
                var data = {
                    id: $(this).data('id')
                }
                if (!engineer_date.is(':disabled')) {
                    data.complete_engineer = engineer_date.val()
                }
                var elem = obj.closest('.jumbotron');
                engineer.elem = elem;
                engineer.processTask(data, elem);

                return false;
            });
        },
        createTask: function (obj, render = false) {
            var data = obj.serialize();
            render = render;
            $.ajax({
                url: 'create-task',
                'type': "POST",
                'data': data,
                success: function (data) {
                    if (render != false) {
                        engineer.renderInCard(data.task);
                    } else {
                        engineer.renderNewTask(data.task);
                    }
                },
                error: function (e) {
                    console.info(e);
                }
            });
        },
        showEngineerInput: function (obj) {
            obj.prop('disabled', function (i, v) {
                return !v;
            });
            obj.slideToggle();
        },
        closeTask: function (id) {
            $.ajax({
                url: 'close-task',
                'type': "POST",
                'data': {
                    id: id
                },
                success: function (data) {
                    engineer.status
                    console.log(data);
                },
                error: function (e) {
                    console.info(e);
                    return;
                }
            });
        },
        processTask: function (data) {
            $.ajax({
                url: 'process-task',
                'type': "POST",
                'data': data,
                success: function (data) {
                    engineer.renderNewTaskDetails(data, engineer.elem);
                },
                error: function (e) {
                    console.info(e);
                    return;
                }
            });
        },
        processTaskCard: function (data) {
            $.ajax({
                url: 'process-task',
                'type': "POST",
                'data': data,
                success: function (data) {
                    engineer.status_ajax = true;
                    engineer.renderProccesstask(data.data);
                },
                error: function (e) {
                    engineer.status_ajax = false;
                    return;
                }
            });
        },
        renderNewTaskDetails: function (data, elem) {
            console.log(data);
            elem.find('.complete_engineer-data').text(data.data.complete_engineer);
            elem.find('.status').text(data.data.status);
        },
        renderNewTask: function (data) {
            var task = '<div class="card"><div class="card-header" role="tab" id="heading_' + data.id + '">' +
                '<h5 class="mb-0"><a data-toggle="collapse" href="#collapse_' + data.id + '" aria-expanded="true" aria-controls="collapse_' + data.id + '" class="">' +
                data.task + '</a></h5></div>' +
                '<div id="collapse_' + data.id + '" class="collapse show" role="tabpanel" aria-labelledby="heading_' + data.id + '" data-parent="#accordion" style="">' +
                '<div class="card-body"><div class="jumbotron"><h1 class="display-5">' + data.task + '</h1>' +
                '<p class="lead">Задачу поставил: ' + data.name + '</p>' +
                '<p class="lead">Задача поставлена: ' + data.start + '</p>' +
                '<p class="lead">Должна быть выполена: ' + data.complete + '</p>' +
                '<p class="lead task-status">Текуший статус: ' + data.status + '</p>';
            if (data.complete_engineer != 'undefined') {
                task += '<p class="lead complete_engineer-data">Конечная дата назначеная инженером: <br>' + data.complete_engineer + '</p>';
            }
            task += '<hr class="my-4"><p class="lead">' +
                '<label for="set-new-date-' + data.id + '">Назначить новую конечную дату <input type="checkbox" id="set-new-date-' + data.id + '" class="show-engineer-date"></label>' +
                '<input type="date" class="complete_engineer form-control" disabled="">' +
                '<a class="btn btn-primary btn-lg process" href="#" role="button" data-id="' + data.id + '">Оброботка</a>' +
                '<a class="btn btn-primary btn-lg close-task" href="#" role="button" data-id="' + data.id + '">Закрыть</a>' +
                '</p></div></div></div></div>';
            //подключаем оброботчики
            $("#accordion_task").prepend(task);
            engineer.bindHendlers();
        },
        renderInCard: function (data) {
            var task = '<div class="row"><div class="col-xl-8 task-text"><p class="">' + data.task + '</p>' +
                '</div><div class="col-xl-3">';
            if (data.user_type == 4) {
                task += '<input type="date" class="form-control task-end-date">' ;
            }
            task += '</div><div class="col-xl-1">';
            if (data.user_type == 4) {
                task += '<button class="btn btn-primary accept-task"> Выполнить</button>';
            }
            task += '</div></div>';
            $(".task-block").append(task);
            $("#new-task-form").val('');
            $(".header-task-waiting").show();
            if (data.user_type == 4) {
                $(".accept-task").on('click', function () {
                    if ($(".task-end-date").val().length < 10) {
                        alert('Введите дату выполнения');
                        return false
                    }
                    var id = $(this).data('taskid');
                    var engineer_complited = $(this).closest('.row').find('.task-end-date').val();
                    var data = {
                        complete_engineer: engineer_complited,
                        id: id
                    };
                    engineer.processTaskCard(data);
                    if (engineer.status_ajax)
                        $(this).closest('.row').remove();
                });
            }

        },
        taskCompleted: function (id) {
            var ajax_res = $.ajax({
                url: 'completed-task',
                'type': "POST",
                'data': {
                    id: id
                },
                success: function (data) {
                    console.log(data);
                },
                error: function (e) {
                    console.info(e);
                    return;
                }
            });
            return ajax_res;
        },
        taskRemove: function (id) {
            var ajax_res = $.ajax({
                url: 'close-task',
                'type': "POST",
                'data': {
                    id: id
                },
                success: function (data) {
                    console.log(data);
                    engineer.status_ajax = true;
                },
                error: function (e) {
                    console.info(e);
                    engineer.status_ajax = false;
                }
            });
            return ajax_res;
        },
        renderProccesstask: function (data) {
            var task = '<div class="container"><div class="row"><div class="col-xl-10">'+
                       '<p><strong>'+data.name+'</strong></p><p>Дата выполнения: '+data.complete_engineer+'</p>'+
                       '<p>'+data.task+'</p></div><div class="col-xl-2 task-in-process-btns">'+
                       '<button class="btn btn-success end-task-card" data-taskid="'+data.id+'">'+
                       'Выполнено</button><button type="button" class="btn btn-dark remove-task-card" data-taskid="'+data.id+'">'+
                       'Отменить</button></div></div></div>';
            $(".task-in-process-card").append(task);
            $(".end-task-card").on('click', function () {
                var task_id = $(this).data('taskid');
                engineer.taskCompleted(task_id);
                $(this).closest('.row').remove();
            });
            $(".remove-task-card").on('click', function () {
                var task_id = $(this).data('taskid');
                engineer.taskRemove(task_id);
                if (engineer.status_ajax)
                    $(this).closest('.row').remove();
            });
        }

    }
}());
$(document).ready(function () {
    engineer.init();
    $("#new-task-engineer").on('submit', function () {
        var obj = $(this);
        engineer.createTask(obj);
        return false;
    });
    engineer.bindHendlers();
    $(".open-task-wrap").on('click', function () {
        $(".tasks-wrap").slideToggle();
    });
    $("#new-task-engineer-card").on('submit', function () {
        var obj = $(this);
        engineer.createTask(obj, 'renderInCard');
        return false;
    });
    $(".end-task-card").on('click', function () {
        var task_id = $(this).data('taskid');
        engineer.taskCompleted(task_id);
        $(this).closest('.row').remove();
    });
    $(".remove-task-card").on('click', function () {
        var task_id = $(this).data('taskid');
        engineer.taskRemove(task_id);
        if (engineer.status_ajax)
            $(this).closest('.row').remove();
    });
    $(".accept-task").on('click', function () {
        if ($(".task-end-date").val().length < 10) {
            alert('Введите дату выполнения');
            return false
        }
        var id = $(this).data('taskid');
        var engineer_complited = $(this).closest('.row').find('.task-end-date').val();
        var data = {
            complete_engineer: engineer_complited,
            id: id
        };
        engineer.processTaskCard(data);
        if (engineer.status_ajax)
            $(this).closest('.row').remove();
    });
});