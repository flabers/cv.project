var filter = (function() {
    return { // методы доступные извне
        //иницыальзацыя отправления токена при каждом аякс запросе
        init: function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                }
            });

        },
        filterBySquare: function (from, to) {
            $.ajax({
               'url': 'user/filter-square',
               'type': "POST",
               'data': {
                   from: from,
                    to: to
               },
                dataType: "JSON",
                success: function (data) {
                    $('.realty-content').empty();
                    filter.renderRealties(data.realties);
                },
                error: function (e) {
                    console.log("Error: "+e);
                }
            });
        },
        filterByMouth: function (month) {
            $.ajax({
                'url': 'user/filter-month',
                'type': "POST",
                'data': {
                    'month': month
                },
                dataType: "JSON",
                success: function (data) {
                    $('.realty-content').empty();
                    filter.renderRealties(data.realties);
                },
                error: function (e) {
                    console.log("Error: "+e);
                }
            });
        },
        simpleFilter: function (obj) {
            var data = obj.serialize();
            try {
                $.ajax({
                    'url': 'user/filter',
                    'type': "POST",
                    'data': data,
                    dataType: "JSON",
                    success: function (data) {
                        $('.realty-content').empty();
                        filter.renderRealties(data.realties);
                    },
                    error: function (e) {
                        console.log("Error: "+e);
                    }
                });
            } catch (e) {
                console.error('Error: '+ e);
                return false;
            }
        },
        renderRealties: function (data) {
            $('.content-table').hide();
            $('.content-table').eq(0).show();
            var j = 1;
            for(var i = 0; i< data.length; i++) {
                var id = data[i].id;
                var str = '<tr><td><a href="/user/card/' + id + '">'+ j +'</a></td>' +
                    '<td><a href="/user/card/'+id+'">'+data[i].address+'</a>' +
                    '</td><td><a href="/user/card/'+id+'">'+data[i].squere +'</a>' +
                    '</td><td><a href="/user/card/'+id+'">'+data[i].type_name +'</a>' +
                    '</td><td><a href="/user/card/'+id+'">'+data[i].tenant_name +'</a>' +
                    '</td><td><a href="/user/card/'+id+'">'+data[i].start_date +'</a>' +
                    '</td><td><a href="/user/card/'+id+'">';
                if (data[i].status_id == 1) {
                    str +=  '<span class="status green "></span>';
                }  else if (data[i].status_id == 2) {
                    str +=  '<span class="status yellow "></span>';
                }  else if (data[i].status_id == 3) {
                    str +=  '<span class="status red "></span>';
                }

                str += '</a></td></tr>';
                $('.realty-content').eq(0).append(str);
                ++j;
            }
            $('.table-header').hide();
        },
        removeFilter: function () {
            $.ajax({
                'url': "user/search",
                'method': 'POST',
                'data': {
                    search: ''
                },
                success: function (data) {
                    $('.realty-content').empty();
                    filter.renderRealties(data.realties);
                },
                error: function (e) {
                    console.log('Error: ' + e);
                }
            });
        }
    }
}());
$(document).ready(function () {
    filter.init();
    $('.filter-by-square').on('click', function () {
       var from = $('.filter-square-input-from').val();
       var to = $('.filter-square-input-to').val();
        filter.filterBySquare(from, to);
        return false;
    });
    $('.filter-by-mouth').on('click', function () {
       var mouth = $('#gMonth2').val();
       filter.filterByMouth(mouth);
    });
    $(".filter-form").on('submit', function () {
       var obj = $(this);
       filter.simpleFilter(obj);
       return false;
    });
    $(".remove-filter").on('click', function () {
        filter.removeFilter();
        $(".filter-form").find("input").val("");
        $(".filter-form").find("input[type=checkbox]").prop('checked', false);
    });
});