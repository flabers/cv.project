var upload = (function () {

    return { // методы доступные извне
        //иницыальзацыя отправления токена при каждом аякс запросе
        init: function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        },
        uploadPictures: function (data) {
            console.log(data);
            $.ajax({
                url: 'picture',
                'type': "POST",
                'data': data,
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                success: function (data) {
                    alert('Фото загружены');
                },
                error: function (e) {
                    console.info(e);
                    return;
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

    }
}());
$(document).ready(function () {
    upload.init();
    // $('#upload-form').on('submit', function(){
    //     // var data = $(this).serialize();
    //     // upload.uploadPictures(data);
    //     $(this).ajaxForm(options);
    //     var options = {
    //         complete: function(response)
    //         {
    //             if($.isEmptyObject(response.responseJSON.error)){
    //                 alert('Image Upload Successfully.');
    //             }else{
    //                 console.log(response.responseJSON.error);
    //             }
    //         }
    //     };
    //     return false;
    // });
});