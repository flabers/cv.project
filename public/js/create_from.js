var create = (function () {
    return {
        //иницыальзацыя отправления токена при каждом аякс запросе
        init: function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('[name="_token"]').val()
                }
            });
        },
        userControl: function (obj) {
            if (obj.hasClass('new')) {
                obj.removeClass('new');
                $(".existing").removeClass('d-none');
                $(".new-tenant").addClass('d-none');
                $(".add-new-tenant").text('Добавить');
                $(".tenant_checker").val('off');
            } else {
                obj.addClass('new');
                $(".new-tenant").removeClass('d-none');
                $(".existing").addClass('d-none');
                $(".add-new-tenant").text('Выбрать существующего');
                $(".tenant_checker").val('on');
            }
        }
    }
}());
$(document).ready(function () {
    $(".add-new-tenant").on('click', function () {
        var obj = $(this);
        create.userControl(obj);

        $(document).animate({
            scrollTop: $(document).height()
        }, 500);
    });
    $("#create-form").validate({
        rules: {
            // simple rule, converted to {required:true}
            realty_name: "required",
            address: "required",
            status: "required",
            type: "required",
            district: "required",
            email: {
                email: true
            },
            phone: {
                regex: /\d+/,
                minlength: 10
            },
            square: {
                required: true,
            },
            // compound rule
            email: {
                required: true,
                email: true
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit();
        }
    });
    $(".existing-tenants").select2();
});